from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles import views

urlpatterns = [
    url(r'^$', 'apps.servicios.views.VistaPlatillos', name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('apps.servicios.urls', namespace='servicios_app')),
    url(r'^', include('apps.estadisticas.urls', namespace='estadisticas_app')),
    url(r'^', include('apps.pagos.urls', namespace='pagos_app')),
    url(r'^', include('apps.usuarios.urls', namespace='usuarios_app')),
    url(r'^', include('apps.catalogos.urls', namespace='catalogos_app')),
]

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    # urlpatterns += patterns('',(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}))
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += [
        url(r'^static/(?P<path>.*)$', views.serve),
    ]
