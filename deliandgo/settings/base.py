import os

from unipath import Path

BASE_DIR = Path(__file__).ancestor(3)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "iuwzedhu6^dj96ti%!h$*ru&(7n+x9i6-6it99xh$)_9)i)9a="

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

# **************    CONFIGURACION DE CORREOS
ADMINS = (('Ricardo_Vera', 'isc4.tec@gmail.com'),)

MANAGERS = (("Ricardo_Vera", "isc4.tec@gmail.com"),)
# """
EMAIL_BACKEND = 'django.core.mail.backends.base.EmailBackend'
DEFAULT_FROM_EMAIL = "contacto@deliandgo.com"
SERVER_EMAIL = "contacto@deliandgo.com"
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.1and1.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'contacto@deliandgo.com'
EMAIL_HOST_PASSWORD = 'Zardoni1208'
"""
SENDGRID_API_KEY = "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"
EMAIL_BACKEND = 'sgbackend.SendGridBackend'
"""
# Application definition

DJANGO_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
)

SITE_ID = 1

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_user_agents.middleware.UserAgentMiddleware',
]

LOCAL_APPS = (
    'apps.catalogos',
    'apps.usuarios',
    'apps.estadisticas',
    'apps.servicios',
    'apps.servicios.templatetags',
    'apps.pagos',
)

THIRD_PARTY_APPS = (
    'geoposition',
    'django_user_agents',
    'carton',
    'haystack',
    'pymysql',

)
CART_PRODUCT_MODEL = 'apps.servicios.models.Platillo'
CART_SESSION_KEY = "NanozCart"
CART_TEMPLATE_TAG_NAME = "carrito"

GEOPOSITION_GOOGLE_MAPS_API_KEY = 'AIzaSyC6UBtxDBeoxSoenu2vBIpgjcuRSk-O29Q'
GOOGLE_MAPS_API_KEY = 'AIzaSyC6UBtxDBeoxSoenu2vBIpgjcuRSk-O29Q'

INSTALLED_APPS = DJANGO_APPS + LOCAL_APPS + THIRD_PARTY_APPS

ROOT_URLCONF = 'deliandgo.urls'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                # 'material.frontend.context_processors.modules',
                'django.core.context_processors.static',
            ],
        },
    },
]
WSGI_APPLICATION = 'deliandgo.wsgi.application'
# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators
'''
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
'''
# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

USE_I18N = True

USE_L10N = True

USE_TZ = True
GEOPOSITION_MAP_OPTIONS = {
    'minZoom': 3,
    'maxZoom': 21,
}

GEOPOSITION_MARKER_OPTIONS = {
    'cursor': 'move'
}
AUTH_USER_MODEL = 'usuarios.Usuario'
LANGUAGE_CODE = 'es-MX'

TIME_ZONE = 'America/Mexico_City'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

DEFAULT_CHARSET = 'utf-8'
FILE_CHARSET = 'utf-8'

PROJECT_ROOT = os.path.normpath(os.path.dirname(__file__))

# media files
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# static files
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATIC_URL = '/static/'
# STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

HAYSTACK_CONNECTIONS = {
    'default': {
        "ENGINE": "haystack.backends.simple_backend.SimpleEngine",
    }
}
'''Nanoz del futuro: cambiar este host por la direccion de arriba cuando se suba'''
HOST_SERVER = "http://localhost:8000/media/"

YELP_CONSUMER_KEY = "QpDM29xyE2K__TPUzW-kdA"
YELP_CONSUMER_SECRET = "VFDGpGgJsWJbkBXLBW2ev-UFEjA"
YELP_TOKEN = "_rNh6Y1fDl4TOnkkryr08f2rq7UyrlwM"
YELP_TOKEN_SECRET = "bLXhCacOQfxKmO4CZb5jAmQ4sp0"
