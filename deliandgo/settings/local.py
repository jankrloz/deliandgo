from .base import *

# pymysql.install_as_MySQLdb()

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'deliandgodb',
        'USER': 'deliandgo',
        'PASSWORD': '123456',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

"""
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'nanozDB',
        'HOST': 'localhost',
        'USER': 'root',
        'PASSWORD': '123456',
        'PORT': '3306',
    }
}
"""

CONEKTA_PRIVATE = "key_7pRAzgJfMq4Gkf64SVYWDA"
CONEKTA_PUBLIC = "key_GNqUyxtVaNkzAXzCVwTvycQ"
