from haystack import indexes

from apps.servicios.models import Platillo


class PlatilloIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    id = indexes.CharField(model_attr="id")
    nombre = indexes.CharField(model_attr="nombre")
    descripcion = indexes.CharField(model_attr="descripcion")
    url_imagen = indexes.CharField(model_attr="get_urlImagen")
    imagen = indexes.CharField(model_attr='(imagen.name).replace(".","")')
    imagen2 = indexes.CharField(model_attr='(imagen.name).replace(".","")')
    tags = indexes.MultiValueField(model_attr="tags")
    text_tags = indexes.CharField(model_attr="tags.objects.all()")

    def get_model(self):
        return Platillo

    def index_queryset(self, using=None):
        return self.get_model().objects.all()

    def get_tags(self, obj):
        return [tag.id for tag in obj.tags.all()]
