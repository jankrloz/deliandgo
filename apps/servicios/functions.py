# -*- encoding: utf-8 -*-
import json
from datetime import date, timedelta

import requests
from bs4 import BeautifulSoup
from django.conf import settings
from django.db.models.aggregates import Sum
from yelp.client import Client
from yelp.oauth1_authenticator import Oauth1Authenticator

from apps.estadisticas.models import Comentarios
from apps.pagos.models import Movimiento
from apps.servicios.models import Tags, ControlNutrimental
from apps.usuarios.models import Restaurante

dias = {
    "Monday": 7,
    "Tuesday": 6,
    "Wednesday": 5,
    "Thursday": 4,
    "Friday": 3,
    "Saturday": 2,
    "Sunday": 1,
}
lista = [
    'display_phone',
    'distance',
    'eat24_url',
    'id',
    'image_url',
    'is_claimed',
    'is_closed',
    'menu_provider',
    'menu_date_updated',
    'mobile_url',
    'name',
    'phone',
    'rating',
    'rating_img_url',
    'rating_img_url_small',
    'rating_img_url_large',
    'reservation_url',
    'review_count',
    'snippet_image_url',
    'snippet_text',
    'url',
]


def lista_tags():
    tags = Tags.objects.all().order_by('nombre')
    lista1 = []
    for tag in tags:
        lista1.append((str(tag.id), str(tag.nombre)))
    return lista1


def lista_tags_principal():
    tags = Tags.objects.all()
    lista1 = []
    for tag in tags:
        lista1.append((str(tag.id), str(tag.nombre)))
    return lista1


def lista_contenido():
    contenidoNutrimental = ControlNutrimental.objects.filter(isPrincipal=False)
    lista1 = []
    for cn in contenidoNutrimental:
        lista1.append((str(cn.id), str(cn.nombre)))
    return lista1


def lista_contenidoTags():
    tags = Tags.objects.all()
    lista1 = []
    for cn in tags:
        lista1.append((str(cn.id), str(cn.nombre)))
    return lista1


def lista_restaurantes():
    restaurantes = Restaurante.objects.all()
    lista = []
    for restaurante in restaurantes:
        lista.append((str(restaurante.id), str(restaurante.contacto.nombre_restaurante)))
    return lista


def buscar_en_yelp(busqueda="", latitudOri=0.0, longitudOri=0.0, restaurante=None):
    auth = Oauth1Authenticator(
            consumer_key=settings.YELP_CONSUMER_KEY,
            consumer_secret=settings.YELP_CONSUMER_SECRET,
            token=settings.YELP_TOKEN,
            token_secret=settings.YELP_TOKEN_SECRET,
    )
    latitud = float(latitudOri)
    longitud = float(longitudOri)
    cliente = Client(auth)
    params = {
        'term': busqueda,
        "lang": "es"
    }
    resultado = cliente.search_by_coordinates(latitud, longitud, **params)
    print(resultado.businesses[0])
    print(resultado.businesses[0].id)
    nombre = str(resultado.businesses[0].name).lower()
    telefono = str(resultado.businesses[0].display_phone).replace("-", "").strip().replace(" ", "").replace("+", "")
    print(nombre)
    print(telefono)
    print(restaurante.contacto.telefono)
    print(busqueda in nombre)
    if busqueda in nombre and restaurante.contacto.telefono in telefono:
        latitudRes = float(resultado.businesses[0].location.coordinate.latitude)
        longitudRes = float(resultado.businesses[0].location.coordinate.longitude)
        print(abs(latitud) - abs(latitudRes))
        print(abs(longitud) - abs(longitudRes))
        variacionLat = abs(abs(latitud) - abs(latitudRes))
        variacionLon = abs(abs(longitud) - abs(longitudRes))
        print(variacionLat < .002)
        print(variacionLon < .002)
        if variacionLat < .002 and variacionLon < .002:
            print(resultado.businesses[0].review_count)
            url = str(resultado.businesses[0].url)
            print(url)
            scraping_yelp_url(url=url, restaurante=restaurante)


'''
    NOTA: esta funcion es la que busca en yelp le pasa la hubicacion(latitud y longitud)
    tambien le pasamos un parametro de busqueda (el nombre del restaurante)
    por ultimo tambien le pasamos el numero de telefono
'''


def buscar_existe_en_yelp(busqueda="", latitudOri=0.0, longitudOri=0.0, telefono1=0):
    # todo: primero nos autentificamos para hacer las peticiones

    auth = Oauth1Authenticator(
            consumer_key=settings.YELP_CONSUMER_KEY,
            consumer_secret=settings.YELP_CONSUMER_SECRET,
            token=settings.YELP_TOKEN,
            token_secret=settings.YELP_TOKEN_SECRET,
    )
    latitud = float(latitudOri)
    longitud = float(longitudOri)
    # todo: luego creamos el cliente y pasamos los parametros de busqueda
    cliente = Client(auth)
    params = {
        'term': busqueda,
        "lang": "es"
    }
    # todo: los resultados los filtramos por coordenadas con una precision de .01
    accuracy = .001
    resultado = cliente.search_by_coordinates(latitud, longitud, accuracy, **params)
    print(resultado.businesses[0])
    print(resultado.businesses[0].id)
    # todo: se optiene el primer resultado que debe empatar con los establecimientos en yelp
    nombre = str(resultado.businesses[0].name).lower()
    telefono = str(resultado.businesses[0].display_phone).replace("-", "").strip().replace(" ", "").replace("+", "")
    print(nombre)
    print(telefono)
    print(telefono1)
    print(busqueda in nombre)
    # todo: comparamos el nombre y el telefono
    if busqueda in nombre and telefono1 in telefono:
        # todo: si se cumplen los criterios de busqueda devolvemos la url de yelp
        print(resultado.businesses[0].review_count)
        url = str(resultado.businesses[0].url)
        print(url)
        return url
    return "no existe en yelp"


# todo entonces empezamos a escanear la url de yelp para obtener los comentarioas
def scraping_yelp_url(url="", restaurante=None):
    if url != "" and restaurante != None:
        req = requests.get(url)
        print(req.status_code)
        x = 0
        # todo: primer vemos si la url es valida
        if req.status_code == 200:
            # todo: extraemos la data de internet
            htmlText = BeautifulSoup(req.text)
            # todo: extraemos la parte donde estan los comentarios
            reviews = htmlText.find_all('div', {'class': 'review review--with-sidebar'})
            # todo: hacemos un ciclo sobre los comentarios
            for review in reviews:
                # todo: tomamos solo los 10 primeros
                if x < 10:
                    # todo: tomamos los datos veridicos de yelp
                    user = review.find('a', {"class": "user-display-name"}).getText()
                    amigos = review.find('li',
                                         {"class": "friend-count responsive-small-display-inline-block"}).b.getText()
                    ubicacion = review.find('li', {"class": "user-location responsive-hidden-small"}).getText()
                    respuesta = review.find('p', {"itemprop": "description"}).getText()
                    puntuacion = review.find('div', {"class": "rating-very-large"}).i["title"]
                    fecha = review.find('span', {"class": "rating-qualifier"}).getText()
                    print("Usuario: " + user)
                    print("Amigos: " + str(amigos))
                    print("origen usuario: " + str(ubicacion).strip())
                    print(puntuacion)
                    print("fecha: " + str(fecha).strip())
                    print("comentario: " + respuesta)
                    # todo: Creamos un comentario con la informacion extraida
                    Comentarios.objects.create(
                            restaurante=restaurante,
                            nombre=user,
                            valoracion=1,
                            texto=respuesta,
                            visible=False
                    )
                x = x + 1


# todo: aveces yelp limita el acceso por eso es que ciclamos la peticion
def busquedaCiclada(restaurante):
    try:
        buscar_en_yelp(
                busqueda=restaurante.contacto.nombre_restaurante,
                latitudOri=restaurante.contacto.direccion.latitude,
                longitudOri=restaurante.contacto.direccion.longitude,
                restaurante=restaurante
        )
    except Exception as e:
        if "Se produjo un error durante el intento de conexión" in str(e.args):
            busquedaCiclada(restaurante)
        else:
            print(e.args)


def movimiento_por_semana(restaurantes, fecha_inicio, fecha_fin, hoy, numero, diccionario):
    restaurante_list = []
    for restaurante in restaurantes:
        restaurante_dict = {}
        movimientos = Movimiento.objects.filter(restaurante=restaurante, fecha__gte=fecha_inicio,
                                                fecha__lte=fecha_fin)
        comision_total = 0
        deposito_total = 0
        lista_movimientos = []
        for movimiento in movimientos:
            comision = round(movimiento.monto * .085, 3)
            balanze = round(movimiento.monto - comision, 2)
            movi = {
                "tipo": movimiento.tipo,
                "fecha": movimiento.fecha.strftime("%d-%b %Y"),
                "direccion": movimiento.pedido.direccion,
                "monto": movimiento.monto,
                "comision ": comision,
                "deposito": balanze,
            }
            comision_total += comision
            deposito_total += balanze
            lista_movimientos.append(movi)
        restaurante_dict["id"] = restaurante.id
        restaurante_dict["restaurante"] = restaurante.get_name
        restaurante_dict["comision-total"] = comision_total
        restaurante_dict["deposito-total"] = deposito_total
        restaurante_dict["pedidos"] = lista_movimientos
        restaurante_list.append(restaurante_dict)
    diccionario[numero] = {
        "titulo": "semana" + str(numero) + " del " + str(fecha_inicio) + " al " + str(fecha_fin),
        "historial": restaurante_list}
    if fecha_fin < hoy:
        movimiento_por_semana(restaurantes, fecha_fin, fecha_fin + timedelta(days=7), hoy, numero + 1, diccionario)
    else:
        return diccionario


def dame_avance_fecha_inicio(fecha_inicio):
    dia = fecha_inicio.strftime("%A")
    return fecha_inicio + timedelta(days=dias[dia])


def calcular_gastos_por_semana():
    restaurantes = Restaurante.objects.all()
    fecha_inicio = Movimiento.objects.earliest("fecha").fecha
    fecha_fin = dame_avance_fecha_inicio(fecha_inicio)
    hoy = date.today()
    diccionario = {}
    movimiento_por_semana(restaurantes, fecha_inicio, fecha_fin, hoy, 1, diccionario)
    print(json.dumps(diccionario, indent=7))
    return diccionario


def dame_semanas_disponibles():
    lista = []
    fecha_inicio = Movimiento.objects.earliest("fecha").fecha
    fecha_fin = dame_avance_fecha_inicio(fecha_inicio)
    hoy = date.today()
    listar_semanas(fecha_inicio, fecha_fin, lista, hoy, 1)
    return lista


def dame_semanas_disponiblesv2(restaurante):
    lista = []
    try:
        fecha_inicio = Movimiento.objects.earliest("fecha").fecha
        fecha_fin = dame_avance_fecha_inicio(fecha_inicio)
        hoy = date.today()
        listar_semanasv2(fecha_inicio, fecha_fin, lista, hoy, 1, restaurante)
    except:
        pass
    return lista


def listar_semanas(fecha_inicio, fecha_fin, lista, hoy, id):
    sumatoria = sumas_movimientos(fecha_inicio, fecha_fin)
    lista.append({
        "id": id,
        "fecha_inicio": fecha_inicio.strftime("%d-%b %Y"),
        "fecha_fin": (fecha_fin - timedelta(days=1)).strftime("%d-%b %Y"),
        "fecha1": fecha_inicio.strftime("%d-%m-%Y"),
        "fecha2": fecha_fin.strftime("%d-%m-%Y"),
        "suma": sumatoria[0],
        "porcentaje": sumatoria[1],
        "cantidad": sumatoria[2],
        "deposito": sumatoria[3],
    })
    if fecha_fin < hoy:
        listar_semanas(fecha_fin, fecha_fin + timedelta(days=7), lista, hoy, id + 1)
    else:
        return lista


def listar_semanasv2(fecha_inicio, fecha_fin, lista, hoy, id, restaurante):
    sumatoria = sumas_movimientosv2(fecha_inicio, fecha_fin, restaurante)
    lista.append({
        "id": id,
        "fecha_inicio": fecha_inicio.strftime("%d-%b %Y"),
        "fecha_fin": (fecha_fin - timedelta(days=1)).strftime("%d-%b %Y"),
        "fecha1": fecha_inicio.strftime("%d-%m-%Y"),
        "fecha2": fecha_fin.strftime("%d-%m-%Y"),
        "suma": sumatoria[0],
        "porcentaje": sumatoria[1],
        "cantidad": sumatoria[2],
        "deposito": sumatoria[3],
    })
    if fecha_fin < hoy:
        listar_semanasv2(fecha_fin, fecha_fin + timedelta(days=7), lista, hoy, id + 1, restaurante)
    else:
        return lista


def sumas_movimientos(fecha_inicio, fecha_fin):
    movimientos = Movimiento.objects.filter(fecha__gte=fecha_inicio, fecha__lte=fecha_fin)
    if movimientos.exists():
        suma = movimientos.aggregate(Sum("monto"))["monto__sum"]
        porcentaje = round(suma * .085, 2)
        cantidad = movimientos.count()
        deposito = suma - porcentaje
    else:
        suma = 0
        porcentaje = 0
        cantidad = 0
        deposito = 0
    return [suma, porcentaje, cantidad, deposito]


def sumas_movimientosv2(fecha_inicio, fecha_fin, restaurante):
    movimientos = Movimiento.objects.filter(fecha__gte=fecha_inicio, fecha__lte=fecha_fin, restaurante=restaurante)
    if movimientos.exists():
        suma = movimientos.aggregate(Sum("monto"))["monto__sum"]
        porcentaje = round(suma * .085, 2)
        cantidad = movimientos.count()
        deposito = suma - porcentaje
    else:
        suma = 0
        porcentaje = 0
        cantidad = 0
        deposito = 0
    return [suma, porcentaje, cantidad, deposito]


def lista_restaurantes_semana(fecha_inicio, fecha_fin):
    restaurantes = Restaurante.objects.all()
    lista = []

    for restaurante in restaurantes:
        movimientosSemana = Movimiento.objects.filter(restaurante=restaurante, fecha__gte=fecha_inicio,
                                                      fecha__lte=fecha_fin)
        if movimientosSemana.exists():
            movimientosSuma = movimientosSemana.filter(tipo="venta")
            movimientosResta = movimientosSemana.filter(tipo="multa")
            print(movimientosSuma)
            print(movimientosResta)
            ventas = 0
            comision = 0
            multas = 0
            deposito = 0
            cantidad = movimientosSuma.count()
            if movimientosSuma.exists():
                print(type(movimientosSuma))
                ventas = movimientosSuma.aggregate(Sum("monto"))["monto__sum"]
                comision = round(ventas * .085, 2)
                try:
                    multas = round(float(movimientosResta.aggregate(Sum("monto"))["monto__sum"]), 2)
                except Exception as e:
                    print(e.args)
                deposito = round((ventas - multas - comision), 2)
            datos = {
                "id": restaurante.id,
                "nombre": restaurante.get_name,
                "depositos": deposito,
                "comision": comision,
                "ventas": ventas,
                "multas": multas,
                "cantidad": cantidad,
            }
            if restaurante.cuenta_bancaria:
                datos["cuenta"] = restaurante.cuenta_bancaria.numero
                datos["banco"] = restaurante.cuenta_bancaria.banco
            else:
                datos["cuenta"] = "0000000000000000"
                datos["banco"] = "Patito"
            lista.append(datos)
    return lista


def listar_movimientos_restaurante_semana(inicio, fin, id):
    movimientosSemana = Movimiento.objects.filter(fecha__gte=inicio, fecha__lte=fin, restaurante__id=id)
    lista_movimientos = []
    for movimiento in movimientosSemana:
        comision = round(movimiento.monto * .085, 2)
        movi = {
            "id": movimiento.id,
            "tipo": movimiento.tipo,
            "fecha": movimiento.fecha.strftime("%d-%b %Y"),
            "direccion": movimiento.pedido.direccion,
        }
        if movimiento.tipo == "multa":
            movi["monto"] = -movimiento.monto,
            movi["comision"] = 0,
            movi["deposito"] = -movimiento.monto,
        else:
            movi["monto"] = movimiento.monto,
            movi["comision"] = -comision,
            movi["deposito"] = movimiento.monto - comision,
        lista_movimientos.append(movi)
    return lista_movimientos
