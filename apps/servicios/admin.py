# -*- encoding: utf-8 -*-
from django.contrib import admin

from .models import ControlNutrimental, Platillo, Pedido


# Register your models here.

@admin.register(ControlNutrimental)
class AdminControlNutrimental(admin.ModelAdmin):
    list_display = (
        'id',
        'nombre'
    )


@admin.register(Platillo)
class AdminPlatillo(admin.ModelAdmin):
    list_display = (
        'nombre',
        'restaurante',
        'precio',
    )
    fields = (
        'nombre',
        'restaurante',
        'descripcion',
        'precio',
        'tiempo_preparacion',
        'hora_inicia',
        'hora_final',
        'control_nutrimental',
        'tags',
        'imagen',
    )

    filter_horizontal = (
        'control_nutrimental',
        'tags',
    )
    readonly_fields = (
        'fecha_hora',
    )


@admin.register(Pedido)
class AdminPedido(admin.ModelAdmin):
    list_display = (
        'usuario',
        'direccion',
    )
    filter_vertical = (
        'platillo',
    )
