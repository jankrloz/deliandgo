from django import forms
from haystack.forms import SearchForm

from apps.catalogos.models import Tags, CuentaBanco
from apps.servicios.functions import lista_tags, lista_contenido, lista_tags_principal, lista_contenidoTags
from apps.servicios.models import Platillo, ControlNutrimental


class PlatilloForm(forms.ModelForm):
    class Meta:
        model = Platillo
        fields = (
            'nombre',
            'precio',
            'tiempo_preparacion',
            'hora_inicia',
            'hora_final',
            'imagen',
            'descripcion',
        )
        labels = {
            'nombre': "Nombre del platillo",
            'precio': "Costo del platillo ($)",
            'tiempo_preparacion': "Tiempo de preparacion (min)",
            'hora_inicia': "Horario de inicio",
            'hora_final': "Horario fin",
            'imagen': "Imagen",
            'descripcion': "Descripcion del platillo",
        }

        widgets = {
            'hora_inicia': forms.TextInput(attrs={
                "type": "time",
                'required': 'true',
                'class': 'form-control timepicker'
            }),
            'hora_final': forms.TextInput(attrs={
                "type": "time",
                'required': 'true',
                'class': 'form-control timepicker'
            }),
            'imagen': forms.FileInput(attrs={
                'class': 'file form-control',
                'accept': 'image/*'
            }),
            'descripcion': forms.Textarea(attrs={
                'maxlength': 240
            })
        }

    tags = forms.ModelMultipleChoiceField(
        queryset=Tags.objects.all()
    )
    tags.label = "Tags platillo"
    tags.required = True

    control = forms.ModelMultipleChoiceField(
        queryset=ControlNutrimental.objects.all()
    )
    control.label = "Control Nutrimental"
    control.required = True


class PlatilloUpdateForm(forms.ModelForm):
    class Meta:
        model = Platillo
        fields = (
            'nombre',
            'tiempo_preparacion',
            'hora_inicia',
            'hora_final',
            'descripcion',
        )
        widgets = {
            'descripcion': forms.Textarea(attrs={
                'maxlength': 240
            })
        }
        labels = {
            'nombre': "Nombre Platillo",
            'tiempo_preparacion': "Tiempo de preparacion",
            'hora_inicia': "Hora I",
            'hora_final': "Hora F",
            'descripcion': "Descripcion del platillo",
        }

    tags = forms.ModelMultipleChoiceField(
        queryset=Tags.objects.all()
    )
    tags.label = "Tags platillo"
    tags.required = True

    control = forms.ModelMultipleChoiceField(
        queryset=ControlNutrimental.objects.all()
    )
    control.label = "Control Nutrimental"
    control.required = True


class PlatilloSearchForm(SearchForm):
    def search(self):
        sqs = super(PlatilloSearchForm, self).search()
        return sqs


class PlatilloSearchFormV2(forms.Form):
    generic = forms.CharField(
        max_length=200,
        label='¿Qúe quieres comer?',
        widget=forms.TextInput(attrs={
            'class': 'form-control input-lg',
            'placeholder': 'Palabras clave'
        })
    )
    tags = forms.ChoiceField(
        widget=forms.CheckboxSelectMultiple,
        choices=lista_tags,
        label="Ingredientes"
    )

    tipo = forms.ChoiceField(
        widget=forms.CheckboxSelectMultiple,
        choices=lista_tags_principal,
        label="¿Qué se te antoja comer?",
    )
    control_nutrimental = forms.ChoiceField(
        widget=forms.CheckboxSelectMultiple,
        choices=lista_contenido,
        label="¿Cómo quieres tu comida?",
    )
    latitud = forms.CharField(
        max_length=200,
        label='latitud',
        widget=forms.TextInput(
            attrs={
                "hidden": True
            }
        )
    )

    longitud = forms.CharField(
        max_length=200,
        label='longitud',
        widget=forms.TextInput(
            attrs={
                "hidden": True
            }
        )
    )


class FavoritosForm(forms.Form):
    cualidad = forms.ChoiceField(
        widget=forms.CheckboxSelectMultiple,
        choices=lista_contenidoTags(),
    )
    cualidad.label = "¿Cómo quieres tu comida?"


class BancoForm(forms.ModelForm):
    class Meta:
        model = CuentaBanco
        fields = (
            'numero',
            'banco',
        )
        labels = {
            'numero': "Numero de la Cuenta Bancaria",
            'banco': "Banco de la Cuenta Bancaria",
        }
