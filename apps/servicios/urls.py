# -*- encoding: utf-8 -*-

from django.conf.urls import url

from apps.servicios.forms import PlatilloSearchForm
from apps.servicios.views import basic_search_nanoz

urlpatterns = [
    url(r'perfil-restaurante/(?P<slug>[\w-]+)/', 'apps.servicios.views.perfilRestaurante', name='perfilRestaurante'),
    url(r'mis-restaurantes/', 'apps.servicios.views.VistaRestaurante', name='verRestaurantes'),
    url(r'restaurante/(?P<slugRestaurante>[\w-]+)/platillo/(?P<slugPlatillo>[\w-]+)/$',
        'apps.servicios.views.detallePlatillo', name='detallePlatillo'),
    url(r'actualizar-platillo/', 'apps.servicios.views.ModificarPlatillo', name='actualizarPlatillo'),
    url(r'cancelar-pedido/', 'apps.servicios.views.cancelar_pedido', name='cancelarPedido'),
    url(r'actualizar-pedido/', 'apps.servicios.views.actualizar_pedido', name='actualizarPedido'),
    url(r'ajaxAgregarFavoritos/$', 'apps.servicios.views.ajaxAgregarFavoritos', name='ajaxAgregarFavoritos'),
    url(r'ajaxAgregarFavoritos2/$', 'apps.servicios.views.ajaxAgregarFavoritos2', name='ajaxAgregarFavoritos2'),
    url(r'ajax_agregar_platillo/$', 'apps.servicios.views.ajaxAgregarPlatillo', name='ajaxAgregarPlatillo'),
    url(r'ajax_quitar_platillo/$', 'apps.servicios.views.ajaxQuitarPlatillo', name='ajaxQuitarPlatillo'),
    url(r'ajax_quitar_1_platillo/$', 'apps.servicios.views.ajaxQuitar1Platillo', name='ajaxQuitar1Platillo'),
    # url(r'^search/', include('haystack.urls')),
    # url(r'^buscador/?$', MySearchView.as_view(), name="buscador"),
    # url(r'^buscador2/?$', buscador2, name="buscador2"),
    url(r'ajaxObtenerSemana/$', 'apps.servicios.views.ajaxObtenerSemana', name='ajaxObtenerSemana'),
    url(r'ajaxObtenerSemanaFacturas/$', 'apps.servicios.views.ajaxObtenerSemanaFacturas',
        name='ajaxObtenerSemanaFacturas'),
    url(r'ajaxObtenerExpecificacionSemana/$', 'apps.servicios.views.ajaxObtenerExpecificacionSemana',
        name='ajaxObtenerExpecificacionSemana'),
    url(r'^ordenar/?$', 'apps.servicios.views.search_with_form', name="buscador2"),
    url(r'^buscadorV2/?$', basic_search_nanoz, {
        'load_all': False,
        "form_class": PlatilloSearchForm,
    }, name="buscador"),

]
