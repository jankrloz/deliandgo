# -*- encoding: utf-8 -*-
import json
import math
from datetime import time, date

from carton.cart import Cart
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.paginator import InvalidPage, Paginator
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import Http404
from django.http.response import HttpResponse
from django.shortcuts import redirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from haystack.forms import ModelSearchForm
from haystack.generic_views import SearchView
from haystack.query import EmptySearchQuerySet
from haystack.query import SearchQuerySet

from apps.catalogos.models import Tags
from apps.estadisticas.models import RestauranteFavoritos, PlatilloFavoritos
from apps.servicios.forms import PlatilloForm, PlatilloUpdateForm, PlatilloSearchForm, PlatilloSearchFormV2, \
    FavoritosForm, BancoForm
from apps.servicios.functions import lista_restaurantes_semana, listar_movimientos_restaurante_semana, \
    dame_semanas_disponiblesv2
from apps.servicios.models import Platillo, ControlNutrimental, Pedido, Favoritos, Facturas
from apps.usuarios.forms import updateInfoRestauranteForm, updateImagenRestauranteForm, updateImagenPlatilloForm, \
    updateUsuarioForm, updateImgUsuarioForm
from apps.usuarios.mailing import correo05
from apps.usuarios.models import Restaurante, Contacto, Usuario

RESULTS_PER_PAGE = getattr(settings, 'HAYSTACK_SEARCH_RESULTS_PER_PAGE', 20)


@login_required
def perfilRestaurante(request, slug):
    print("entrando ala vista de platillos")
    print(slug)
    print("GET")
    print(request.GET)
    print("POST")
    print(request.POST)
    print("FILES")
    print(request.FILES)
    restaurantes = Restaurante.objects.filter(slug=slug, duenio=request.user)
    print(restaurantes)
    if restaurantes.exists():
        print("el restaurante si existe")
        restaurante = restaurantes.first()
        if request.method == "POST":
            formPlatillo = PlatilloForm(request.POST, request.FILES)
            formRestaurante = updateInfoRestauranteForm(request.POST, request.FILES)
            formImgRestaurante = updateImagenRestauranteForm(request.POST, request.FILES)
            formImgPlatillo = updateImagenPlatilloForm(request.POST, request.FILES)
            formBanco = BancoForm(request.POST)
            print(formPlatillo.is_valid())
            print(formRestaurante.is_valid())
            print(formImgRestaurante.is_valid())
            print(formImgPlatillo.is_valid())
            print(formBanco.is_valid())
            if formBanco.is_valid():
                banco = formBanco.save()
                restaurante.cuenta_bancaria = banco
                restaurante.save()
            if formPlatillo.is_valid():
                try:
                    nuevoPlatillo = formPlatillo.save(commit=False)
                    nuevoPlatillo.restaurante = restaurante
                    nuevoPlatillo.save()
                    lista = []
                    for id in request.POST.getlist('tags'):
                        lista.append(int(id))
                    try:
                        nuevoPlatillo.tags = Tags.objects.filter(id__in=lista)
                    except Exception as e:
                        print("Erro al guardar los tags")
                        print(e.args)
                    lista = []
                    if 'control' in request.POST:
                        for id in request.POST.getlist('control'):
                            lista.append(int(id))
                    try:
                        control = ControlNutrimental.objects.filter(id__in=lista)
                        nuevoPlatillo.control_nutrimental = control
                    except Exception as e:
                        print("Error al guardar los tags")
                        print(e.args)
                    nuevoPlatillo.save()
                    return redirect(reverse('servicios_app:perfilRestaurante', kwargs={
                        'slug': restaurante.slug
                    }))
                except Exception as e:
                    print(e)
            if formRestaurante.is_valid():
                restaurante.servicio = request.POST["servicio"]
                restaurante.alcanze = request.POST["alcanze"]
                restaurante.tags = Tags.objects.filter(id__in=request.POST.getlist("tags"))
                restaurante.compra_minima = request.POST["compra_minima"]
                restaurante.costo_envio = request.POST["costo_envio"]
                try:
                    if "hora_abre" in request.POST:
                        hora = str(request.POST["hora_abre"]).split(":")
                        restaurante.hora_abre = time(int(hora[0]), int(hora[1]), int(hora[2]))
                    if "hora_cierra" in request.POST:
                        hora = str(request.POST["hora_cierra"]).split(":")
                        restaurante.hora_cierra = time(int(hora[0]), int(hora[1]), int(hora[2]))
                except:
                    pass
                restaurante.save()
                contacto = Contacto.objects.get(id=restaurante.contacto.id)
                contacto.email = request.POST["email"]
                contacto.telefono = request.POST["telefono"]
                contacto.nombre_restaurante = request.POST["nombre_restaurante"]

            if formImgRestaurante.is_valid():
                restaurante.imagen = request.FILES["imagen"]
                restaurante.save()
            if formImgPlatillo.is_valid():
                platilloTemp = Platillo.objects.get(id=request.POST["id_platillo"], restaurante=restaurante)
                platilloTemp.imagen = request.FILES["imagen"]
                platilloTemp.save()
        platillos = Platillo.objects.filter(restaurante=restaurante)
        print(platillos)
        pedidos = Pedido.objects.filter(platillo__restaurante=restaurante).exclude(estado="en cargo").distinct()
        semanas = dame_semanas_disponiblesv2(restaurante)
        data = {
            "nombre": restaurante.contacto.nombre_responsable,
            "telefono": restaurante.contacto.telefono,
            "email": restaurante.contacto.email,
            "compra_minima": restaurante.compra_minima,
            "alcanze": restaurante.alcanze,
            "hora_cierra": restaurante.hora_cierra,
            "hora_abre": restaurante.hora_abre,
            "costo_envio": restaurante.costo_envio,
        }
        formRestaurante = updateInfoRestauranteForm(data=data)
        formImgRestaurante = updateImagenRestauranteForm(data={
            "slug": slug,
        })
        formPlatillo = PlatilloForm()
        for platillo in platillos:
            platillo.formUpdateInfo = PlatilloUpdateForm(instance=platillo)
            platillo.formUpdateImg = updateImagenPlatilloForm(data={
                "id_platillo": platillo.id,
            })
        formBanco = BancoForm()
        return render(request, "servicios/perfilRestaurante.html", {
            "form_platillo": formPlatillo,
            "form_restaurante": formRestaurante,
            "form_imgRestaurante": formImgRestaurante,
            "slug": slug,
            "restaurante": restaurante,
            "platillos": platillos,
            "pedidos": pedidos,
            "semanas": semanas,
            "form_Banco": formBanco,

        })
    else:
        print("no se encontro ningun restaurante se le enviara al home")
        return redirect(reverse('index'))


def VistaPlatillos(request):
    Platillos = Platillo.objects.all()
    return render(request, "servicios/VistaPlatillos.html", {
        "platillos": Platillos
    })


@login_required
def VistaRestaurante(request):
    user = request.user
    if user.is_restaurante:
        restaurantes = Restaurante.objects.filter(duenio=user)
        if restaurantes.exists():
            return render(request, "servicios/listaRestaurantes.html", {
                "restaurantes": restaurantes
            })
    return redirect(reverse('index'))


@login_required
def ModificarPlatillo(request):
    print("get")
    print(request.GET)
    print("post")
    print(request.POST)
    print("files")
    print(request.FILES)
    print("fin de request")
    if request.method == "POST":
        if "slug" in request.POST:
            try:
                restaurante = Restaurante.objects.get(slug=request.POST["slug"])
                platillo = Platillo.objects.get(id=request.POST["id"])
                platillo.descripcion = request.POST["descripcion"]
                platillo.control_nutrimental = request.POST.getlist("control")
                platillo.tags = request.POST.getlist("tags")
                platillo.nombre = request.POST["nombre"]
                try:
                    if "hora_inicia" in request.POST:
                        hora = str(request.POST["hora_inicia"]).split(":")
                        platillo.hora_inicia = time(int(hora[0]), int(hora[1]), int(hora[2]))
                    if "hora_final" in request.POST:
                        hora = str(request.POST["hora_final"]).split(":")
                        platillo.hora_final = time(int(hora[0]), int(hora[1]), int(hora[2]))
                    platillo.tiempo_preparacion = int(float(request.POST["tiempo_preparacion"]))
                except:
                    pass
                platillo.save()
                return redirect(reverse('servicios_app:perfilRestaurante', kwargs={
                    'slug': restaurante.slug
                }))
            except:
                pass
    return redirect(reverse('index'))


@login_required
def PerfilUsuario(request):
    print("GET")
    print(request.GET)
    print("POST")
    print(request.POST)
    print("FILES")
    print(request.FILES)
    print("fin del request")
    usuario = Usuario.objects.get(id=request.user.id)
    estilos = Tags.objects.all()
    if request.method == "POST":
        formInfoUpdate = updateUsuarioForm(request.POST)
        formImgUpdate = updateImgUsuarioForm(request.POST, request.FILES)
        # formNewTarjeta = tarjetaForm(request.POST)
        # formTarjetaUpdate = updateTarjetaForm(request.POST)
        # formFavoritos = FavoritosForm(request.POST, request.FILES)
        # print(formFavoritos.is_valid())
        print("POST: ", request.POST)
        if "tipo" in request.POST:
            print("tipo en post")
            if Favoritos.objects.filter(usuario=request.user).exists():
                favorito = Favoritos.objects.get(
                        usuario=request.user
                )
            else:
                favorito = Favoritos.objects.create(
                        usuario=request.user
                )
            favorito.estilos = request.POST.getlist("tipo")
            favorito.save()
            return redirect(reverse('usuarios_app:Perfil'))
        if formInfoUpdate.is_valid():
            usuario.nombre = request.POST["nombre"]
            usuario.apellidos = request.POST["apellidos"]
            usuario.telefono = request.POST["telefono"]
            usuario.save()
            return redirect(reverse('usuarios_app:Perfil'))
        if formImgUpdate.is_valid():
            usuario.avatar = request.FILES["imagen"]
            usuario.save()
            return redirect(reverse('usuarios_app:Perfil'))
        '''
        if formTarjetaUpdate.is_valid():
            tarjeta = Tarjeta.objects.get(
                    id=int(float(request.POST["id_tarjeta"])),
                    usuario=request.user
            )
            tarjeta.nombre = request.POST["nombre"]
            tarjeta.numero = int(float(request.POST["numero"]))
            tarjeta.numero_secreto = int(float(request.POST["numero_secreto"]))
            tarjeta.mes = int(float(request.POST["mes"]))
            tarjeta.anio = int(float(request.POST["anio"]))
            tarjeta.save()
            return redirect(reverse('Perfil'))
        else:
            if formNewTarjeta.is_valid():
                tarjeta = formNewTarjeta.save(commit=False)
                tarjeta.usuario = usuario
                tarjeta.save()
                return redirect(reverse('Perfil'))
        '''
    formImgUpdate = updateImgUsuarioForm(
            data={
                "id_usuario": usuario.id,
            }
    )
    formInfoUpdate = updateUsuarioForm(
            data={
                "nombre": usuario.nombre,
                "apellidos": usuario.apellidos,
                "telefono": usuario.telefono
            }
    )
    '''
    formNewTarjeta = tarjetaForm()
    misTarjetas = Tarjeta.objects.filter(usuario=request.user)
    for tarjeta in misTarjetas:
        tarjeta.form = updateTarjetaForm(
                data={
                    "id_tarjeta": tarjeta.id,
                    "nombre": tarjeta.nombre,
                    "numero": tarjeta.numero,
                    "numero_secreto": tarjeta.numero_secreto,
                    "mes": tarjeta.mes,
                    "anio": tarjeta.anio,
                }
        )
    '''
    misPedidos = Pedido.objects.filter(usuario=int(request.user.id)).exclude(estado="en cargo")
    misFavoritos = []
    if Favoritos.objects.filter(usuario=request.user).exists():
        favorito = Favoritos.objects.get(usuario=request.user)
        misFavoritos = favorito.estilos.all().values_list('nombre', flat=True)
    formFavoritos = FavoritosForm()
    for mipedido in misPedidos:
        mipedido.NombreRestaurante = mipedido.platillo.all().first().restaurante.contacto.nombre_restaurante
    likesRestaurnates = RestauranteFavoritos.objects.filter(usuario=request.user)
    likesPlatillos = PlatilloFavoritos.objects.filter(usuario=request.user)
    for x in formFavoritos:
        print(vars(x))
        for y in x:
            print(vars(y))
            print(y.choice_label)
    return render(request, "usuarios/perfilUsuario.html", {
        "form": formInfoUpdate,
        "formImgUpdate": formImgUpdate,
        "formFavoritos": formFavoritos,
        "misPedidos": misPedidos,
        "estilos": estilos,
        "likesRestaurnates": likesRestaurnates,
        "likesPlatillos": likesPlatillos,
        # "misTarjetas": misTarjetas,
        "misFavoritos": misFavoritos,
        # "formNewTarjeta": formNewTarjeta,
    })


@csrf_exempt
def ajaxAgregarPlatillo(request):
    print("entrando ala vista de platillos")
    print("GET")
    print(request.GET)
    print("POST")
    print(request.POST)
    print("ajax?")
    print(request.is_ajax())
    if request.is_ajax():
        if request.method == 'POST':
            id_platillo = int(request.POST["id_platillo"])
            if Platillo.objects.filter(id=id_platillo).exists():
                platillo = Platillo.objects.get(id=id_platillo)
                verificarRestarurante(platillo.restaurante, request)
                try:
                    carrito = Cart(session=request.session, session_key="nanozcarrito")
                    carrito.add(platillo, platillo.precio)
                    print(carrito.items)
                    print(carrito.total)
                except Exception as e:
                    print(e.args)
                    return HttpResponse(json.dumps(e.args), content_type="application/json")
            try:
                platillos, envio, total = obtener_carrito_json(request)
                data = {
                    "platillos": platillos,
                    "costoEnvio": envio,
                    "total": total
                }
                print(data)
                return HttpResponse(json.dumps(data), content_type="application/json")
            except Exception as e:
                return HttpResponse(json.dumps(e.args), content_type="application/json")
    return HttpResponse("ok")


@csrf_exempt
def ajaxQuitarPlatillo(request):
    print("entrando ala vista de platillos")
    print("GET")
    print(request.GET)
    print("POST")
    print(request.POST)
    print("ajax?")
    print(request.is_ajax())
    if request.is_ajax():
        if request.method == 'POST':
            id_platillo = int(request.POST["id_platillo"])
            if Platillo.objects.filter(id=id_platillo).exists():
                try:
                    platilo = Platillo.objects.get(id=id_platillo)
                    carrito = Cart(session=request.session, session_key="nanozcarrito")
                    carrito.remove(platilo)
                    print(carrito.items)
                    print(carrito.total)
                except Exception as e:
                    print(e.args)
                    return HttpResponse(json.dumps(e.args), content_type="application/json")
            try:
                platillos, envio, total = obtener_carrito_json(request)
                data = {
                    "platillos": platillos,
                    "costoEnvio": envio,
                    "total": total
                }
                print(data)
                return HttpResponse(json.dumps(data), content_type="application/json")
            except Exception as e:
                return HttpResponse(json.dumps(e.args), content_type="application/json")
    return HttpResponse("ok")


@csrf_exempt
def ajaxQuitar1Platillo(request):
    print("entrando ala vista de platillos")
    print("GET")
    print(request.GET)
    print("POST")
    print(request.POST)
    print("ajax?")
    print(request.is_ajax())
    if request.is_ajax():
        if request.method == 'POST':
            id_platillo = int(request.POST["id_platillo"])
            if Platillo.objects.filter(id=id_platillo).exists():
                try:
                    platilo = Platillo.objects.get(id=id_platillo)
                    carrito = Cart(session=request.session, session_key="nanozcarrito")
                    carrito.remove_single(platilo)
                    print(carrito.items)
                    print(carrito.total)
                except Exception as e:
                    print(e.args)
                    return HttpResponse(json.dumps(e.args), content_type="application/json")
            try:
                platillos, envio, total = obtener_carrito_json(request)
                data = {
                    "platillos": platillos,
                    "costoEnvio": envio,
                    "total": total
                }
                print(data)
                return HttpResponse(json.dumps(data), content_type="application/json")
            except Exception as e:
                return HttpResponse(json.dumps(e.args), content_type="application/json")
    return HttpResponse("ok")


def detallePlatillo(request, slugRestaurante, slugPlatillo):
    print("view detalle del platillo")
    print("get")
    print(request.GET)
    print("post")
    print(request.POST)
    print("fin de los request")
    platillos = Platillo.objects.filter(slug=slugPlatillo, restaurante__slug=slugRestaurante)
    if platillos.exists():
        platillo = platillos.first()
        otrosPlatillos = Platillo.objects.filter(
                restaurante=platillo.restaurante
        ).exclude(
                id=platillo.id
        )
        restaurante = Restaurante.objects.get(
                id=platillo.restaurante.id
        )
        return render(request, "servicios/detallePlatillo.html", {
            "platillo": platillo,
            "otrosPlatillos": otrosPlatillos,
            "restaurante": restaurante,
        })
    return redirect(reverse('index'))


@login_required
def cancelar_pedido(request):
    if request.method == "POST":
        if "slug" in request.POST and "id_pedido" in request.POST:
            slug = request.POST["slug"]
            pedido = Pedido.objects.get(id=request.POST["id_pedido"])
            pedido.estado = "cancelado"
            pedido.save()
            user = Usuario.objects.get(id=pedido.usuario)
            correo05(destino=[user.email, ])
            return redirect(
                    reverse('servicios_app:perfilRestaurante', kwargs={
                        'slug': slug
                    }))
    return redirect(reverse('index'))


@login_required
def actualizar_pedido(request):
    if request.method == "POST":
        if "slug" in request.POST and "id_pedido" in request.POST:
            slug = request.POST["slug"]
            pedido = Pedido.objects.get(id=request.POST["id_pedido"])
            if pedido.estado == "pendiente":
                pedido.estado = "en cocina"
            elif pedido.estado == "en cocina":
                pedido.estado = "en camino"
            elif pedido.estado == "en camino":
                pedido.estado = "entregado"
            pedido.save()
            return redirect(
                    reverse('servicios_app:perfilRestaurante', kwargs={
                        'slug': slug
                    }))
    return redirect(reverse('index'))


class MySearchView(SearchView):
    def get_queryset(self):
        queryset = super(MySearchView, self).get_queryset()
        return queryset.filter(titulo__icontains="#1")

    def get_context_data(self, *args, **kwargs):
        contex = super(MySearchView, self).get_context_data(*args, **kwargs)
        return contex


class buscador2(SearchView):
    template_name = "servicios/buscador.html"
    queryset = SearchQuerySet().load_all()
    form_class = PlatilloSearchForm(data={})


def basic_search_nanoz(request, template='search/search.html', load_all=True, form_class=ModelSearchForm,
                       searchqueryset=None, extra_context=None, results_per_page=None):
    """
    A more traditional view that also demonstrate an alternative
    way to use Haystack.

    Useful as an example of for basing heavily custom views off of.

    Also has the benefit of thread-safety, which the ``SearchView`` class may
    not be.

    Template:: ``search/search.html``
    Context::
        * form
          An instance of the ``form_class``. (default: ``ModelSearchForm``)
        * page
          The current page of search results.
        * paginator
          A paginator instance for the results.
        * query
          The query received by the form.
    """
    query = ''
    results = EmptySearchQuerySet()

    if request.GET.get('q'):
        form = form_class(request.GET, searchqueryset=searchqueryset, load_all=load_all)

        if form.is_valid():
            query = form.cleaned_data['q']
            results = form.search()
    else:
        form = form_class(searchqueryset=searchqueryset, load_all=load_all)

    paginator = Paginator(results, results_per_page or RESULTS_PER_PAGE)

    try:
        page = paginator.page(int(request.GET.get('page', 1)))
    except InvalidPage:
        raise Http404("No such page of results!")

    context = {
        'form': form,
        'page': page,
        'paginator': paginator,
        'query': query,
        'suggestion': None,
    }

    if results.query.backend.include_spelling:
        context['suggestion'] = form.get_suggestion()

    if extra_context:
        context.update(extra_context)

    return render(request, template, context)


def search_with_form(request):
    request.GET = request.GET.copy()
    request.GET['1'] = '2'
    print(request.GET)
    busquedaForm = PlatilloSearchFormV2(request.GET)

    estilos = Tags.objects.all()

    restaurantes2 = None
    if 'longitud' in request.GET and 'latitud' in request.GET:
        if request.GET['latitud'] != "" and request.GET['longitud'] != "":
            lat = float(request.GET['latitud'])
            long = float(request.GET['longitud'])

            # Haversine formula = https://en.wikipedia.org/wiki/Haversine_formula
            R = 6378.1  # earth radius
            distance = 5  # distance in km
            restaurantes = Restaurante.objects.all()
            lista = []

            for restaurante in restaurantes:
                if restaurante.contacto.direccion:
                    latitud = restaurante.contacto.direccion.latitude
                    longitud = restaurante.contacto.direccion.longitude

                    distancia = R * math.acos(math.cos(math.radians(lat)) * math.cos(math.radians(latitud)) * math.cos(
                            math.radians(longitud) - math.radians(long)) + math.sin(math.radians(lat)) * math.sin(
                            math.radians(latitud)))
                    print(distancia)
                    if distancia < distance:
                        lista.append(restaurante.id)
            if len(lista) > 0:
                platillosUbicacion = Platillo.objects.filter(restaurante_id__in=lista)
                print(platillosUbicacion.count())
            else:
                return render(request, "servicios/buscadorV2.html", {
                    "busquedaForm": busquedaForm,
                    "restaurantes": [],
                    "estilos": estilos,
                    "mensajeError": "Lo sentimos mucho pero no hay ningun restaurante cerca de tu hubicacion",
                })

            if "generic" in request.GET:
                if request.GET["generic"] != "":
                    generic = str(request.GET["generic"]).strip()
                    platillo1 = platillosUbicacion.filter(
                            Q(nombre__icontains=generic) |
                            Q(descripcion__icontains=generic) |
                            Q(restaurante__contacto__nombre_restaurante__icontains=generic) |
                            Q(restaurante__contacto__nombre_responsable__icontains=generic)
                    )
                    platillosUbicacion = platillo1
            if "tipo" in request.GET:
                tags = request.GET.getlist("tipo")
                if tags != "":
                    platillo2 = platillosUbicacion.filter(
                            tags__in=tags
                    )
                    platillosUbicacion = platillo2

            if len(platillosUbicacion) > 0:
                platillos = platillosUbicacion.distinct()
                restaurantes2 = Restaurante.objects.filter(
                        id__in=platillos.values_list('restaurante', flat=True).distinct())
                for restaurante in restaurantes2:
                    lista = []
                    ids = []
                    for platillo in platillos:
                        if platillo.restaurante == restaurante:
                            lista.append(platillo)
                            ids.append(platillo.id)
                    for platillo2 in lista:
                        platillo2.otrosIDs = ids
                    restaurante.filtrados = lista
                    restaurante.cantidad = len(lista)
                restaurantes2.order_by('comentarioAvg')
                restaurantes2 = sorted(restaurantes2, key=lambda x: x.cantidad, reverse=True)
            else:
                restaurantes2 = Restaurante.objects.all()
        else:
            restaurantes2 = Restaurante.objects.all()
    else:
        restaurantes2 = Restaurante.objects.all()
    return render(request, "servicios/buscadorV2.html", {
        "busquedaForm": busquedaForm,
        "restaurantes": restaurantes2,
        "estilos": estilos,
        "mensajeError": "No hemos encontrado ningun platillo con tus criterios de busqueda, prueba una convinacion diferente",
    })


def obtener_carrito_json(request):
    carrito = request.session["nanozcarrito"]
    total = 0
    costoEnvio = 0
    listaPlatillosJson = []
    for x in carrito:
        print(carrito[x])
        id_platillo = carrito[x]["product_pk"]
        platillo = Platillo.objects.get(id=id_platillo)
        cantidad = carrito[x]["quantity"]
        precio = platillo.precio
        subTotal = cantidad * precio
        data = {
            'id': str(id_platillo).replace('"', "'"),
            'nombre': str(platillo.nombre).replace('"', "'"),
            'cantidad': str(cantidad).replace('"', "'"),
            'precio': str(precio).replace('"', "'"),
            'subtotal': str(subTotal).replace('"', "'"),
        }
        print("imprimiendo data " + str(x))
        print(data)
        total += subTotal
        listaPlatillosJson.append(data)
        costoEnvio = float(platillo.restaurante.costo_envio)
    print("data completa")
    print(listaPlatillosJson)
    return listaPlatillosJson, costoEnvio, total


@csrf_exempt
def ajaxObtenerSemana(request):
    if request.is_ajax():
        if request.POST:
            list_fin = str(request.POST["fin"]).split("-")
            list_inicio = str(request.POST["inicio"]).split("-")
            fin = date(
                    year=int(list_fin[2]),
                    month=int(list_fin[1]),
                    day=int(list_fin[0])
            )
            inicio = date(
                    year=int(list_inicio[2]),
                    month=int(list_inicio[1]),
                    day=int(list_inicio[0])
            )
            data = lista_restaurantes_semana(inicio, fin)
            return HttpResponse(json.dumps(data), content_type="application/json")
        return HttpResponse("ok")
    return HttpResponse("error")


@csrf_exempt
def ajaxObtenerSemanaFacturas(request):
    if request.is_ajax():
        if request.method == "POST":
            list_fin = str(request.POST["fin"]).split("-")
            list_inicio = str(request.POST["inicio"]).split("-")
            fin = date(
                    year=int(list_fin[2]),
                    month=int(list_fin[1]),
                    day=int(list_fin[0])
            )
            inicio = date(
                    year=int(list_inicio[2]),
                    month=int(list_inicio[1]),
                    day=int(list_inicio[0])
            )
            facturas = Facturas.objects.filter(fecha__gte=inicio, fecha__lte=fin)
            lista = []
            for factura in facturas:
                lista.append({
                    "id": factura.id,
                    "fecha": factura.fecha,
                    "restaurante": factura.restaurante.get_name,
                    "url": factura.imagen.url
                })
            return HttpResponse(json.dumps(lista), content_type="application/json")
        return HttpResponse("ok")
    return HttpResponse("error")


@csrf_exempt
def ajaxObtenerExpecificacionSemana(request):
    if request.is_ajax():
        if request.POST:
            print(request.POST)

            list_fin = str(request.POST["fin"]).split("-")
            list_inicio = str(request.POST["inicio"]).split("-")
            id = request.POST["id"]
            if request.user == Restaurante.objects.get(id=id).duenio or request.user.is_superuser:
                fin = date(
                        year=int(list_fin[2]),
                        month=int(list_fin[1]),
                        day=int(list_fin[0])
                )
                inicio = date(
                        year=int(list_inicio[2]),
                        month=int(list_inicio[1]),
                        day=int(list_inicio[0])
                )
                data = listar_movimientos_restaurante_semana(inicio, fin, id)
                return HttpResponse(json.dumps(data), content_type="application/json")
            else:
                return ("error")
        return HttpResponse("ok")
    return HttpResponse("error")


def verificarRestarurante(newRestaurante, request):
    if "nanozcarrito" in request.session:
        carrito = request.session["nanozcarrito"]
        for platillo in carrito:
            id_platillo = int(carrito[platillo]["product_pk"])
            restaurante = Platillo.objects.get(id=id_platillo).restaurante
            print(restaurante, newRestaurante)
            if restaurante != newRestaurante:
                try:
                    carrito = Cart(session=request.session, session_key="nanozcarrito")
                    carrito.clear()
                except Exception as e:
                    print(e.args)
                break


@csrf_exempt
def ajaxAgregarFavoritos(request):
    if request.is_ajax():
        if request.method == "POST":
            id_restaurante = int(request.POST["id_restaurante"])
            user = request.user.id
            if not RestauranteFavoritos.objects.filter(usuario__id=user, restaurante__id=id_restaurante).exists():
                restaurante = Restaurante.objects.get(id=id_restaurante)
                usuario = Usuario.objects.get(id=user)
                RestauranteFavoritos.objects.create(
                        restaurante=restaurante,
                        usuario=usuario
                )
            else:
                return "no"
    return HttpResponse("ok")


@csrf_exempt
def ajaxAgregarFavoritos2(request):
    if request.is_ajax():
        if request.method == "POST":
            id_platillo = int(request.POST["id_platillo"])
            user = request.user.id
            if not PlatilloFavoritos.objects.filter(usuario__id=user, platillo__id=id_platillo).exists():
                platillo = Platillo.objects.get(id=id_platillo)
                usuario = Usuario.objects.get(id=user)
                PlatilloFavoritos.objects.create(
                        platillo=platillo,
                        usuario=usuario
                )
            else:
                return "no"
    return HttpResponse("ok")
