# -*- encoding: utf-8 -*-
import random
import string

from django.db import models
from django.utils.text import slugify

from apps.catalogos.models import Tags
from apps.usuarios.models import Restaurante, Usuario, Direccion


def directorio_img_platillo(instance, filename):
    return "restaurante/{id}/platillos/{file}".format(id=instance.restaurante.slug, file=filename)


def directorio_img_factura(instance, filename):
    return "restaurante/{id}/facturas/{file}".format(id=instance.restaurante.slug, file=filename)


class ControlNutrimental(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre

    def __unicode__(self):
        return self.nombre


class Platillo(models.Model):
    nombre = models.CharField(max_length=50)
    restaurante = models.ForeignKey(Restaurante)
    control_nutrimental = models.ManyToManyField(ControlNutrimental)
    tags = models.ManyToManyField(Tags)
    imagen = models.FileField(upload_to=directorio_img_platillo)
    precio = models.FloatField(default=0.0)
    tiempo_preparacion = models.IntegerField()
    hora_inicia = models.TimeField()
    hora_final = models.TimeField()
    fecha_hora = models.DateTimeField(auto_now_add=True)
    descripcion = models.TextField(max_length=240)
    slug = models.CharField(max_length=100)

    @property
    def get_urlImagen(self):
        return self.imagen._get_url

    @property
    def urlImagen(self):
        if self.imagen:
            return self.imagen._get_url

    def __str__(self):
        return self.nombre

    def __unicode__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        if not self.id:
            slug = slugify(self.nombre)
            cantidad = Platillo.objects.filter(slug__icontains=slug).count()
            slug = slugify(slug[:50] + "-" + str(cantidad))
            self.slug = slug
        super(Platillo, self).save(*args, **kwargs)


class Pedido(models.Model):
    platillo = models.ManyToManyField(Platillo)
    usuario = models.IntegerField(default=0)
    direccion = models.TextField(max_length=4000)
    monto_total = models.FloatField(default=0.0)
    lista_estado = (
        ("en cargo", "en cargo"),
        ("pendiente", "pendiente"),
        ("en cocina", "en cocina"),
        ("en camino", "en camino"),
        ("entregado", "entregado"),
        ("cancelado", "cancelado"),
    )
    estado = models.CharField(max_length=10, choices=lista_estado, default="pendiente")
    fecha = models.DateTimeField(auto_now_add=True)
    nombre_recibe = models.CharField(max_length=200)
    detalles = models.TextField(max_length=4000)
    llave = models.TextField(max_length=50)

    def __str__(self):
        return str(self.usuario) + str(self.direccion)

    def __unicode__(self):
        return str(self.usuario) + str(self.direccion)

    def save(self, *args, **kwargs):
        if not self.id:
            self.llave = genera_codigo()
        super(Pedido, self).save(*args, **kwargs)


class Favoritos(models.Model):
    usuario = models.ForeignKey(Usuario)
    ingedientes = models.ManyToManyField(Tags, related_name="ingredientes")
    estilos = models.ManyToManyField(Tags, related_name="estilos")
    cualidad = models.ManyToManyField(ControlNutrimental)

    def __unicode__(self):
        return "Favoritos de : " + str(self.usuario.get_full_name)

    def __str__(self):
        return "Favoritos de : " + str(self.usuario.get_full_name)


def genera_codigo():
    codigo_prueba = cadena_aleatorio(50)
    while existe_codigo(codigo_prueba):
        codigo_prueba = cadena_aleatorio(50)
    return codigo_prueba


def cadena_aleatorio(tamanio):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(tamanio))


def existe_codigo(codigo):
    return Pedido.objects.filter(llave=codigo).exists()


class Facturas(models.Model):
    restaurante = models.ForeignKey(Restaurante)
    fecha = models.DateField(null=False, blank=False)
    imagen = models.FileField(upload_to=directorio_img_factura)
