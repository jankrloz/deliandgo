from django import template
from django.conf import settings
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.simple_tag(takes_context=True)
def total_carrito(context):
    request = context['request']
    if 'nanozcarrito' in request.session:
        carrito = request.session["nanozcarrito"]
        counter = 0
        for key, value in carrito.items():
            counter += int(value['quantity'])
        # print('carrito: ', counter)
        return counter
    else:
        return 0


@stringfilter
@register.filter()
def replacePunto(value):
    print(value)
    # campos = str(value).split(".")
    host = settings.HOST_SERVER
    return str(host) + str(value)
