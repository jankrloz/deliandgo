# -*- encoding: utf-8 -*-
from django.apps import AppConfig


class EstadisticasConfig(AppConfig):
    name = 'apps.estadisticas'
