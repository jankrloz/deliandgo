# -*- encoding: utf-8 -*-
from django.db import models

from apps.catalogos.models import Pregunta
from apps.servicios.models import Platillo
from apps.usuarios.models import Restaurante, Usuario

#todo: si un comentario tiene como usuario=0 es por que fue extraido desde yelp!
class Comentarios(models.Model):
    restaurante = models.ForeignKey(Restaurante)
    usuario = models.IntegerField(default=0)
    nombre = models.CharField(max_length=100)
    fecha = models.DateTimeField(auto_now_add=True)
    CALIFICAIONES = ((5, '5'), (4, '4'), (3, '3'), (2, '2'), (1, '1'),)
    valoracion = models.IntegerField(choices=CALIFICAIONES, null=True)
    texto = models.TextField()
    visible = models.BooleanField(default=True)

    def __unicode__(self):
        return ""

    def __str__(self):
        return ""

    class Meta:
        verbose_name = "Comentario"
        verbose_name_plural = "Comentarios"


class Metadatos(models.Model):
    ip = models.CharField(max_length=20)
    usuario = models.IntegerField(default=0)
    dia_hora = models.DateTimeField(auto_now_add=True)
    pagina_visitada = models.URLField()
    proyecto = models.IntegerField(default=0)
    sistema_operativo = models.CharField(max_length=100)
    navegador = models.CharField(max_length=30)
    pagina_procedencia = models.URLField()
    dispositivo = models.CharField(max_length=20)

    def __str__(self):
        if self.usuario != 0:
            try:
                return Usuario.objects.get(id=self.usuario).username
            except:
                return 'anonimo'
        else:
            return 'Anonimo'

    def __unicode__(self):
        if self.usuario != 0:
            try:
                return Usuario.objects.get(id=self.usuario).username
            except:
                return 'anonimo'
        else:
            return 'Anonimo'

    @property
    def username(self):
        if self.usuario != 0:
            try:
                return Usuario.objects.get(id=self.usuario).username
            except:
                return 'anonimo'
        else:
            return 'Anonimo'


class Respuesta(models.Model):
    pregunta = models.ForeignKey(Pregunta)
    respuesta = models.CharField(max_length=300)


class TestNecesidad(models.Model):
    usuario = models.ForeignKey(Usuario)
    respuesta = models.ManyToManyField(Respuesta)
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "cuestionario de " + self.usuario

    def __unicode__(self):
        return "cuestionario de " + self.usuario


class RestauranteFavoritos(models.Model):
    usuario = models.ForeignKey(Usuario)
    restaurante = models.ForeignKey(Restaurante)

    def __str__(self):
        return str(self.restaurante.get_name) + " - " + str(self.usuario.nombre)

    def __unicode__(self):
        return str(self.restaurante.get_name) + " - " + str(self.usuario.nombre)


class PlatilloFavoritos(models.Model):
    usuario = models.ForeignKey(Usuario)
    platillo = models.ForeignKey(Platillo)

    def __str__(self):
        return str(self.platillo.nombre) + " - " + str(self.usuario.nombre)

    def __unicode__(self):
        return str(self.platillo.nombre) + " - " + str(self.usuario.nombre)
