from django import forms

from apps.estadisticas.models import Comentarios


class ComentraioForm(forms.Form):
    calificacion = forms.ChoiceField(
        widget=forms.RadioSelect,
        choices=Comentarios.CALIFICAIONES
    )
    calificacion.required = True
    calificacion.label = "Calificación"
    comentario = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}))
    comentario.required = True
    comentario.label = "Comentario"
