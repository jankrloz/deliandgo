# -*- encoding: utf-8 -*-
from django.contrib import admin

from .models import Comentarios, Metadatos, TestNecesidad


# Register your models here.
@admin.register(Comentarios)
class AdminComentarios(admin.ModelAdmin):
    list_display = (
        'restaurante',
        'usuario',
        'valoracion',
    )


@admin.register(Metadatos)
class AdminMetadatos(admin.ModelAdmin):
    list_display = (
        'ip',
        'usuario',
        'pagina_visitada',
        'pagina_procedencia',
    )


@admin.register(TestNecesidad)
class adminTestNecesidad(admin.ModelAdmin):
    list_display = (
        "usuario",
        'fecha',
    )
    fields = (
        "usuario",
        'fecha',
        'respuesta',
    )
    readonly_fields = (
        'fecha',
    )
    filter_horizontal = (
        'respuesta',
    )
