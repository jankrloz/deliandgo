# -*- encoding: utf-8 -*-
from django.contrib import admin

from apps.pagos.models import Movimiento
from .models import Pagos, Tarjeta


# Register your models here.

@admin.register(Pagos)
class AdminPagos(admin.ModelAdmin):
    list_display = (
        'pedido',
        'tarjeta',
        'monto',
    )


@admin.register(Tarjeta)
class AdminTarjeta(admin.ModelAdmin):
    list_display = (
        'usuario',
        'nombre',
        'numero',
    )


@admin.register(Movimiento)
class AdminMovimiento(admin.ModelAdmin):
    pass
