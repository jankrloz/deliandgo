# -*- encoding: utf-8 -*-
from django.db import models

from apps.servicios.models import Pedido
from apps.usuarios.models import Usuario, Restaurante


class Tarjeta(models.Model):
    usuario = models.ForeignKey(Usuario, null=False)
    nombre = models.CharField(max_length=100)
    numero = models.CharField(max_length=20)
    numero_secreto = models.CharField(max_length=3)
    banco = models.CharField(max_length=20)
    # fecha expiracion
    mes = models.CharField(max_length=2)
    anio = models.CharField(max_length=4)
    fecha = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return "tarjeta " + str(self.id) + " " + str(self.usuario.get_full_name)

    def __str__(self):
        return "tarjeta " + str(self.id) + " " + str(self.usuario.get_full_name)


class Pagos(models.Model):
    pedido = models.ForeignKey(Pedido)
    tarjeta = models.ForeignKey(Tarjeta)
    monto = models.FloatField(default=0.0)
    idConekta = models.CharField(max_length=1000)
    referencia = models.CharField(max_length=100)
    jsonPeticion = models.TextField()
    jsonRespuesta = models.TextField()
    codigo = models.CharField(max_length=50, blank=True, null=True)

    def __unicode__(self):
        return self.pedido + " - " + self.tarjeta

    def __str__(self):
        return self.pedido + " - " + self.tarjeta


class Movimiento(models.Model):
    restaurante = models.ForeignKey(Restaurante)
    pedido = models.ForeignKey(Pedido, null=True)
    TIPO_MOVIMIENTOS = (
        ("venta", "venta"),
        ("deposito", "deposito"),
        ("multa", "multa"),
        ("comision", "comision"),
    )
    tipo = models.CharField(choices=TIPO_MOVIMIENTOS, max_length=10)
    monto = models.FloatField(default=0.0)
    fecha = models.DateField()

    def __unicode__(self):
        return str(self.restaurante) + " " + str(self.tipo) + " " + str(self.monto) + " " + str(self.fecha)

    def __str__(self):
        return str(self.restaurante) + " " + str(self.tipo) + " " + str(self.monto) + " " + str(self.fecha)
