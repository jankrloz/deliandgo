# -*- encoding: utf-8 -*-

from django.conf.urls import url

urlpatterns = [
    url(r'^carrito/$', 'apps.pagos.views.VerCarrito', name='VerCarrito'),
    # url(r'^preOrden/$', 'apps.pagos.views.preOrden', name='preOrden'),
    url(r'^preOrden2/$', 'apps.pagos.views.preOrdenV2', name='preOrdenv2'),
    url(r'^pagar/$', 'apps.pagos.views.pagar', name='pagar'),
    url(r'^crearCuenta/$', 'apps.pagos.views.crearCuentaV2', name='crearCuentaV2'),
    url(r'^testTarjeta/$', 'apps.pagos.views.testTarjeta', name='testTarjeta'),

]
