# -*- encoding: utf-8 -*-
import datetime

import conekta
from django.conf import settings
from django.contrib.auth import login
from django.core.urlresolvers import reverse
from django.http.response import Http404
from django.shortcuts import render, redirect

from apps.pagos.forms import updateTarjetaForm, tarjetaForm, formDatosPedido
from apps.pagos.models import Tarjeta, Movimiento
from apps.servicios.models import Platillo, Pedido
from apps.usuarios.forms import updateDireccionForm, direccionForm, SignUpForm
from apps.usuarios.mailing import correo02, correo09
from apps.usuarios.models import Usuario, Direccion, EmailOrUsernameModelBackend


def VerCarrito(request):
    total = 0
    lista = []
    lista2 = []
    subTotalCarrito = 0
    costoEnvio = 0
    if request.method == "POST":
        carito = request.session["nanozcarrito"]
        for x in carito:
            print(carito[x])
            id_platillo = carito[x]["product_pk"]
            print(id_platillo)
            titulo = "cambios" + str(id_platillo)
            if titulo in request.POST:
                request.session[titulo] = request.POST[titulo]
            print(titulo)
            print(request.session[titulo])
        return redirect(reverse("pagos_app:preOrdenv2"))
    try:
        carito = request.session["nanozcarrito"]
        print(carito)
        print(type(carito))
        listaCambios = []
        for x in carito:
            print(carito[x])
            id_platillo = int(carito[x]["product_pk"])
            platillo = Platillo.objects.get(id=id_platillo)
            platillo.cantidad = int(carito[x]["quantity"])
            subTotal = platillo.cantidad * platillo.precio
            platillo.subTotal = subTotal
            titulo = "cambios" + str(id_platillo)
            if titulo in request.session:
                platillo.cambios = request.session[titulo]
            total += subTotal
            lista.append(platillo)
            subTotalCarrito += subTotal
        lista2 = sorted(lista, key=lambda platillo: platillo.id)
        costoEnvio = platillo.restaurante.costo_envio
        total += costoEnvio
    except Exception as e:
        print("error en el carrito")
        print(e.args)
    return render(request, "pagos/carrito.html", {
        "carrito": lista2,
        "total": total,
        "subTotalCarrito": subTotalCarrito,
        "constoEnvio": costoEnvio,
    })


'''
def preOrden(request):
    print("get")
    print(request.GET)
    print("post")
    print(request.POST)
    print("seccion")
    print(request.session)
    print("fin de los request")
    print("nanozcarrito" in request.session)
    if ("nanozcarrito" in request.session):
        carito = request.session["nanozcarrito"]
        for x in carito:
            print(carito[x])
            id_platillo = carito[x]["product_pk"]
            print(id_platillo)
            titulo = "cambios" + str(id_platillo)
            if titulo in request.POST:
                request.session[titulo] = request.POST[titulo]
            print(titulo)
            print(request.session[titulo])
    else:
        return redirect(reverse('index'))
    registrado = request.user.is_authenticated()
    data = {
        "user_register": SignUpForm(),
        "registrado": registrado,
    }
    if registrado:
        usuario = Usuario.objects.get(id=request.user.id)

        tarjetas = Tarjeta.objects.filter(
                usuario=usuario
        )

        for tarjeta in tarjetas:
            tarjeta.updateFormTar = updateTarjetaForm(
                    data={
                        "id_tarjeta": tarjeta.id,
                        "nombre": tarjeta.nombre,
                        "numero": tarjeta.numero,
                        "numero_secreto": tarjeta.numero_secreto,
                        "mes": tarjeta.mes,
                        "anio": tarjeta.anio,
                    }
            )
        data["tarjetas"] = tarjetas

        direcciones = Direccion.objects.filter(
                usuario=usuario
        )

        for direccion in direcciones:
            direccion.updateFormDir = updateDireccionForm(
                    data={
                        "id_direccion": direccion.id,
                        "calle": direccion.calle,
                        "numero_ext": direccion.numero_ext,
                        "numero_int": direccion.numero_int,
                        "estado": direccion.estado,
                        "CP": direccion.CP,
                        "referencia": direccion.referencia,
                    }
            )
        data["direcciones"] = direcciones
    if request.method == "POST":
        newTarjeta = tarjetaForm(request.POST)
        newDireccion = direccionForm(request.POST)
        updateTarjeta = updateTarjetaForm(request.POST)
        updateDireccion = updateDireccionForm(request.POST)
        if updateTarjeta.is_valid():
            try:
                actulizar_tarjeta(request)
                return redirect(reverse("pagos_app:preOrden"))
            except Exception as e:
                print(e.args)
        else:
            if newTarjeta.is_valid():
                crear_tarjeta(request)
                return redirect(reverse("pagos_app:preOrden"))
            else:
                print("error creando nueva tarjeta")
                print(str(newTarjeta.errors))
        if updateDireccion.is_valid():
            try:
                actualizar_tarjeta(request)
                return redirect(reverse("pagos_app:preOrden"))
            except Exception as e:
                print(e.args)
        else:
            if newDireccion.is_valid():
                crear_direccion(request)
                return redirect(reverse("pagos_app:preOrden"))
            else:
                print("error creando nueva direccion")
                print(str(newDireccion.errors))

    newTarjeta = tarjetaForm()
    newDireccion = direccionForm()
    if "errorTarjeta" in request.session:
        data["error"] = request.session["errorTarjeta"]
        del request.session["errorTarjeta"]
    data["newTarjeta"] = newTarjeta
    data["newDireccion"] = newDireccion
    data["conektaLlavePublica"] = settings.CONEKTA_PUBLIC
    return render(request, "pagos/preOrden.html", data)
'''
'''
def pagar(request):
    print("get")
    print(request.GET)
    print("post")
    print(request.POST)
    print("seccion")
    print(request.session)
    print("fin de los request")
    print("nanozcarrito" in request.session)
    error = []
    if "nanozcarrito" in request.session and request.method == "POST":
        platillos, costoEnvio, total, detalles, visible = obtener_carrito(request)
        if "pedidoId" in request.session:
            print("se encontro el pedido en seccion")
            print(request.session["pedidoId"])
            pedido = Pedido.objects.get(llave=request.session["pedidoId"])
            print(pedido.id)
        else:
            print("no se encontro el pedido en seccion se creara uno nuevo")
            pedido = Pedido.objects.create(
                    monto_total=total + costoEnvio,
                    estado="en cargo",
                    nombre_recibe=request.POST["nombre_recibe"],
                    detalles=detalles
            )
            request.session["pedidoId"] = pedido.llave
        pedido.platillo = platillos
        pedido.save()
        if request.user.is_authenticated():
            print("usuario autentificado")
            if "id_tarjeta_select" in request.POST and "id_direccion_select" in request.POST:
                # if "id_direccion_select" in request.POST:
                direccion = Direccion.objects.get(id=request.POST["id_direccion_select"])
                print(type(direccion))
                print(vars(direccion))
                if "conektaTokenId" in request.POST:
                    try:
                        conekta.api_key = settings.CONEKTA_PRIVATE
                        conekta.locale = "es"
                        usuario = Usuario.objects.get(id=request.user.id)
                        lista_de_platillos = []
                        pedido.direccion = direccion.get_full
                        pedido.usuario = usuario.id
                        pedido.save()
                        for platillo in pedido.platillo.all():
                            print(platillo)
                            dicionario = {
                                "name": platillo.nombre,
                                "description": platillo.descripcion,
                                "unit_price": platillo.precio * 100,
                                "quantity": 1,
                                "type": "comida"
                            }
                            lista_de_platillos.append(dicionario)
                        charge = conekta.Charge.create({
                            "description": "Comida a domicilio",
                            "amount": pedido.monto_total * 100,
                            "currency": "MXN",
                            "reference_id": pedido.llave,
                            "card": request.POST["conektaTokenId"],
                            "details": {
                                "name": usuario.get_full_name,
                                "phone": usuario.get_telefono,
                                "email": usuario.email,
                                "line_items": lista_de_platillos,
                                "billing_address": {
                                    "street1": direccion.calle,
                                    "street2": direccion.numero_ext,
                                    "street3": direccion.numero_int,
                                    "city": "DistritoFederal",
                                    "state": direccion.estado,
                                    "zip": direccion.CP,
                                    "country": "Mexico",
                                    "phone": usuario.get_full_name,
                                    "email": usuario.email
                                },
                                "shipment": {
                                    "carrier": "privado",
                                    "service": "local",
                                    "price": 0,
                                    "address": {
                                        "street1": "250 Alexis St",
                                        "street2": "Interior 303",
                                        "street3": "Col. Condesa",
                                        "city": "Red Deer",
                                        "state": "Alberta",
                                        "zip": "T4N 0B8",
                                        "country": "Canada"
                                    }
                                }
                            }
                        })
                        print(charge.status)
                        print(vars(charge))
                        print("se va a borrar el carrito y el pedido")
                        del request.session["nanozcarrito"]
                        del request.session["pedidoId"]
                        crear_movimiento_pedido(llave=pedido.llave)
                        return render(request, "pagos/pagar.html", {
                            'error': error
                        })
                    except conekta.ConektaError as e:
                        error.append(e.error_json["message_to_purchaser"])
                        print(e.error_json["message_to_purchaser"])
                    except Exception as e:
                        error.append(str(e.args))
                        print(e.args)
            else:
                print("no se eligio la direccion o tarjeta")
        else:
            user_register = SignUpForm(request.POST)
            newDireccion = direccionForm(request.POST)
            print(user_register.is_valid())
            print(newDireccion.is_valid())
            if user_register.is_valid():
                Usuario.objects.create_user(
                        username=user_register.cleaned_data['email'].lower().strip().replace(' ', '_'),
                        email=user_register.cleaned_data['email'].lower(),
                        password=user_register.cleaned_data['password'],
                        is_active=True)
                autentificar = EmailOrUsernameModelBackend()
                user = autentificar.authenticate(
                        username=user_register.cleaned_data['email'].lower().strip().replace(' ', '_'),
                        password=user_register.cleaned_data['password'])
                if user is not None:
                    user.backend = 'django.contrib.auth.backends.ModelBackend'
                    login(request, user)
                    if newDireccion.is_valid():
                        direccion = crear_direccion(request)
                        try:
                            direccion2 = "Recibe: " + request.POST["nombre_recibe"] + " Principal " + request.POST[
                                "calle"] + " No. Ext." + request.POST["numero_ext"] + " Referencia: " + request.POST[
                                             "referencia"] + " email: " + request.POST["email"]
                            if "conektaTokenId" in request.POST:
                                try:
                                    conekta.api_key = settings.CONEKTA_PRIVATE
                                    conekta.locale = "es"
                                    pedido.direccion = direccion.get_full
                                    pedido.usuario = user.id
                                    pedido.save()
                                    lista_de_platillos = []
                                    for platillo in pedido.platillo.all():
                                        print(platillo)
                                        dicionario = {
                                            "name": platillo.nombre,
                                            "description": platillo.descripcion,
                                            "unit_price": platillo.precio * 100,
                                            "quantity": 1,
                                            "type": "comida"
                                        }
                                        lista_de_platillos.append(dicionario)
                                    charge = conekta.Charge.create({
                                        "description": "Comida a domicilio",
                                        "amount": pedido.monto_total * 100,
                                        "currency": "MXN",
                                        "reference_id": pedido.llave,
                                        "card": request.POST["conektaTokenId"],
                                        "details": {
                                            "name": request.POST["nombre_recibe"],
                                            "phone": request.POST["celular"],
                                            "email": request.POST["email"],
                                            "line_items": lista_de_platillos,
                                            "billing_address": {
                                                "street1": request.POST["calle"],
                                                "street2": request.POST["numero_ext"],
                                                "city": "DistritoFederal",
                                                "country": "Mexico",
                                                "phone": request.POST["nombre_recibe"],
                                                "email": request.POST["email"]
                                            },
                                            "shipment": {
                                                "carrier": "privado",
                                                "service": "local",
                                                "price": 0,
                                                "address": {
                                                    "street1": "250 Alexis St",
                                                    "street2": "Interior 303",
                                                    "street3": "Col. Condesa",
                                                    "city": "Red Deer",
                                                    "state": "Alberta",
                                                    "zip": "T4N 0B8",
                                                    "country": "Canada"
                                                }
                                            }
                                        }
                                    })
                                    print(charge.status)
                                    print(vars(charge))
                                    form = SignUpForm(data={"email": request.POST["email"]})
                                    print("se va a borrar el carrito y el pedido")
                                    del request.session["nanozcarrito"]
                                    del request.session["pedidoId"]
                                    # crear_movimiento_pedido(llave=pedido.llave)
                                    return render(request, "pagos/pagar.html", {
                                        "Form": form,
                                        'error': error
                                    })
                                except conekta.ConektaError as e:
                                    error.append(e.error_json["message_to_purchaser"])
                                    print(e.error_json["message_to_purchaser"])
                                except Exception as e:
                                    error.append(str(e.args))
                                    print(e.args)
                            else:
                                pass
                        except Exception as e:
                            print("faltan campos")
                            error.append(str(e.args))
                    else:
                        print(newDireccion.errors)
            else:
                print(user_register.errors)
    return redirect(reverse('pagos_app:preOrden'))
'''


def pagar(request):
    return render(request, "pagos/pagar.html", {})


def crearCuentaV2(request):
    if request.method == "POST":
        user_register = SignUpForm(request.POST)
        print(user_register.is_valid())
        if user_register.is_valid():
            Usuario.objects.create_user(
                    username=user_register.cleaned_data['email'].lower().strip().replace(' ', '_'),
                    email=user_register.cleaned_data['email'].lower(),
                    password=user_register.cleaned_data['password'],
                    is_active=True)
            autentificar = EmailOrUsernameModelBackend()
            user = autentificar.authenticate(
                    username=user_register.cleaned_data['email'].lower().strip().replace(' ', '_'),
                    password=user_register.cleaned_data['password'])
            if user is not None:
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                login(request, user)
                return redirect(reverse("usuarios_app:Perfil"))
        else:
            raise Http404("la informacion para crear usuario no es valida")
    else:
        raise Http404("intentado crear usuario sin post")


def testTarjeta(request):
    print("inicia el GET")
    print(request.GET)
    print("inicia el POST")
    print(request.POST)
    print("fin del request")
    conektaLlavePublica = settings.CONEKTA_PUBLIC
    error = ""
    if request.method == "POST":
        if "conektaTokenId" in request.POST:

            try:
                conekta.api_key = settings.CONEKTA_PRIVATE
                conekta.locale = "es"
                pedido = Pedido.objects.all().first()

                usuario = Usuario.objects.all().first()
                lista_de_platillos = []
                for platillo in pedido.platillo.all():
                    print(platillo)
                    dicionario = {
                        "name": platillo.nombre,
                        "description": platillo.descripcion,
                        "unit_price": platillo.precio * 100,
                        "quantity": 1,
                        "type": "comida"
                    }
                    lista_de_platillos.append(dicionario)
                direccion = Direccion.objects.all().last()
                charge = conekta.Charge.create({
                    "description": "Comida a domicilio",
                    "amount": pedido.monto_total * 100,
                    "currency": "MXN",
                    "reference_id": pedido.llave,
                    "card": request.POST["conektaTokenId"],
                    "details": {
                        "name": usuario.get_full_name,
                        "phone": usuario.get_telefono,
                        "email": usuario.email,
                        "line_items": lista_de_platillos,
                        "billing_address": {
                            "street1": direccion.calle,
                            "street2": direccion.numero_ext,
                            "street3": direccion.numero_int,
                            "city": "DistritoFederal",
                            "state": direccion.estado,
                            "zip": direccion.CP,
                            "country": "Mexico",
                            "phone": usuario.get_telefono,
                            "email": usuario.email
                        },
                        "shipment": {
                            "carrier": "privado",
                            "service": "local",
                            "price": 0,
                            "address": {
                                "street1": "250 Alexis St",
                                "street2": "Interior 303",
                                "street3": "Col. Condesa",
                                "city": "Red Deer",
                                "state": "Alberta",
                                "zip": "T4N 0B8",
                                "country": "Canada"
                            }
                        }
                    }
                })
                print(charge.status)
                print(vars(charge))
            except conekta.ConektaError as e:
                error = str(e.error_json["message_to_purchaser"])
                print(e.error_json["message_to_purchaser"])
            except Exception as e:
                error = str(e.args)
                print(e.args)
    return render(request, "pagos/cobroTarjeta.html", {
        "conektaLlavePublica": conektaLlavePublica,
        "error": error,
    })


def crear_tarjeta(request):
    newTarjeta = tarjetaForm(request.POST)
    tarjetaNueva = newTarjeta.save(commit=False)
    tarjetaNueva.usuario = request.user
    tarjetaNueva.save()
    print("se creo una tarjeta nueva")


def actulizar_tarjeta(request):
    updateTarjeta = updateTarjetaForm(request.POST)
    id_tarjeta = updateTarjeta.cleaned_data["id_tarjeta"]
    tarjetaUpdate = Tarjeta.objects.get(id=id_tarjeta, usuario=request.user)
    tarjetaUpdate.nombre = updateTarjeta.cleaned_data["nombre"]
    tarjetaUpdate.numero = updateTarjeta.cleaned_data["numero"]
    tarjetaUpdate.numero_secreto = updateTarjeta.cleaned_data["numero_secreto"]
    tarjetaUpdate.banco = updateTarjeta.cleaned_data["banco"]
    tarjetaUpdate.mes = updateTarjeta.cleaned_data["mes"]
    tarjetaUpdate.anio = updateTarjeta.cleaned_data["anio"]
    tarjetaUpdate.save()
    print("se actualizo la tarjeta #" + str(id_tarjeta))


def crear_direccion(request):
    newDireccion = direccionForm(request.POST)
    direccionNueva = newDireccion.save(commit=False)
    direccionNueva.usuario = request.user
    direccionNueva.save()
    print("se creo una nueva direccion")
    return direccionNueva


def actualizar_tarjeta(request):
    updateDireccion = updateDireccionForm(request.POST)
    id_direccion = updateDireccion.cleaned_data["id_direccion"]
    direccionUpdate = Direccion.objects.get(id=id_direccion, usuario=request.user)
    direccionUpdate.calle = updateDireccion.cleaned_data["calle"]
    direccionUpdate.numero_ext = updateDireccion.cleaned_data["numero_ext"]
    direccionUpdate.numero_int = updateDireccion.cleaned_data["numero_int"]
    direccionUpdate.estado = updateDireccion.cleaned_data["estado"]
    direccionUpdate.CP = updateDireccion.cleaned_data["CP"]
    direccionUpdate.referencia = updateDireccion.cleaned_data["referencia"]
    direccionUpdate.save()
    print("se actualizo la direccion " + str(id_direccion))


def obtener_carrito(request):
    carrito = request.session["nanozcarrito"]
    total = 0
    costoEnvio = 0
    print(carrito)
    print(type(carrito))
    lista = []
    detalles = ""
    for x in carrito:
        print(carrito[x])
        id_platillo = int(carrito[x]["product_pk"])
        platillo = Platillo.objects.get(id=id_platillo)
        platillo.cantidad = int(carrito[x]["quantity"])
        subTotal = platillo.cantidad * platillo.precio
        platillo.subTotal = subTotal
        titulo = "cambios" + str(id_platillo)
        if titulo in request.session:
            platillo.cambios = request.session[titulo]
            detalles += str(request.session[titulo])
        total += subTotal
        lista.append(platillo)
        costoEnvio = float(platillo.restaurante.costo_envio)
    lista2 = sorted(lista, key=lambda platillo: platillo.id)
    platillos = lista
    return platillos, costoEnvio, total, detalles, lista2


def crear_movimiento_pedido(llave=""):
    pedido = Pedido.objects.get(llave=llave)
    pedido.estado = "pendiente"
    pedido.save()
    fecha = datetime.date.today()
    try:
        Movimiento.objects.create(
                restaurante=pedido.platillo.all().first().restaurante,
                pedido=pedido,
                tipo="venta",
                monto=pedido.monto_total,
                fecha=fecha
        )
    except Exception as e:
        print(e)


def preOrdenV2(request):
    print("get")
    print(request.GET)
    print("post")
    print(request.POST)
    print("fin del request")
    error = []
    if not "nanozcarrito" in request.session:
        return redirect(reverse("servicios_app:buscador2"))
    platillos, costoEnvio, total, detalles, carrito = obtener_carrito(request)
    if request.method == "POST":
        datosPedidoForm = formDatosPedido(request.POST)
        if datosPedidoForm.is_valid():
            print(datosPedidoForm.is_valid())
            idUsuario = 0
            if request.user.is_authenticated():
                idUsuario = request.user.id
            if request.user.is_anonymous() and "passNuevo" in request.POST:
                password = str(request.POST["passNuevo"])
                if len(password) > 5:
                    mailNuevo = str(request.POST["email"]).lower().strip().replace(' ', "_")
                    userNuevo = Usuario.objects.create_user(
                            username=mailNuevo,
                            email=mailNuevo,
                            password=password
                    )
                    autentificar = EmailOrUsernameModelBackend()
                    user = autentificar.authenticate(
                            username=mailNuevo,
                            password=password)
                    if user is not None:
                        user.backend = 'django.contrib.auth.backends.ModelBackend'
                        login(request, user)
                        idUsuario = userNuevo.id
                    user.nombre = request.POST["nombre"]
                    user.apellidos = request.POST["apellidos"]
                    user.telefono = request.POST["telefono"]
                    user.save()
            if "conektaTokenId" in request.POST:
                if "pedidoId" in request.session:
                    print("se encontro el pedido en seccion")
                    print(request.session["pedidoId"])
                    pedido = Pedido.objects.get(llave=request.session["pedidoId"])
                    print(pedido.id)
                else:
                    print("no se encontro el pedido en seccion se creara uno nuevo")
                    pedido = Pedido.objects.create(
                            monto_total=total + costoEnvio,
                            estado="en cargo",
                            nombre_recibe=request.POST["nombre"] + " " + request.POST["apellidos"],
                            detalles=detalles
                    )
                    request.session["pedidoId"] = pedido.llave
                try:
                    conekta.api_key = settings.CONEKTA_PRIVATE
                    conekta.locale = "es"
                    pedido.direccion = \
                        "ciudad: " + request.POST["ciudad_1"] + " colonia: " + request.POST["colonia_1"] + " calle " + \
                        request.POST["direccion"] + " No. " + request.POST["numeroInt"] + "referencias: " + \
                        request.POST["referencia"] + " \nEntregar a:" + request.POST["nombre"] + " " + request.POST[
                            "apellidos"] + " \nContacto celular: " + request.POST["telefono"] + " correo: " + \
                        request.POST["email"]

                    lista_de_platillos = []
                    platillos_list = []
                    for platillo in platillos:
                        print(platillo)
                        platillos_list.append(platillo.id)
                        dicionario = {
                            "name": platillo.nombre,
                            "description": platillo.descripcion,
                            "unit_price": platillo.precio * 100,
                            "quantity": 1,
                            "type": "comida"
                        }
                        lista_de_platillos.append(dicionario)
                    pedido.platillo = Platillo.objects.filter(id__in=platillos_list)
                    pedido.usuario = idUsuario
                    pedido.save()
                    charge = conekta.Charge.create({
                        "description": "Comida a domicilio",
                        "amount": pedido.monto_total * 100,
                        "currency": "MXN",
                        "reference_id": pedido.llave,
                        "card": str(request.POST["conektaTokenId"]),
                        "details": {
                            "name": str(request.POST["nombre"]) + " " + str(request.POST["apellidos"]),
                            "phone": int(request.POST["telefono"]),
                            "email": str(request.POST["email"]),
                            "line_items": lista_de_platillos,
                            "billing_address": {
                                "street1": str(request.POST["direccion"]),
                                "street2": str(request.POST["colonia_1"]),
                                "city": str(request.POST["ciudad_1"]),
                                "country": "Mexico",
                                "phone": int(request.POST["telefono"]),
                                "email": str(request.POST["email"])
                            },
                            "shipment": {
                                "carrier": "privado",
                                "service": "local",
                                "price": 0,
                                "address": {
                                    "street1": "250 Alexis St",
                                    "street2": "Interior 303",
                                    "street3": "Col. Condesa",
                                    "city": "Red Deer",
                                    "state": "Alberta",
                                    "zip": "T4N 0B8",
                                    "country": "Canada"
                                }
                            }
                        }
                    })
                    print(charge.status)
                    print(vars(charge))
                    print("se va a borrar el carrito y el pedido")
                    del request.session["nanozcarrito"]
                    del request.session["pedidoId"]
                    pedido.estado = "pendiente"
                    pedido.save()
                    correo02(destino=[str(request.POST["email"]), ])
                    correo09(destino=[pedido.platillo.all().first().restaurante.duenio.email, ])
                    crear_movimiento_pedido(llave=pedido.llave)
                    return render(request, "pagos/pagar.html", {})
                except conekta.ConektaError as e:
                    print("error conekta")
                    error.append(e.error_json["message_to_purchaser"])
                    print(e.error_json["message_to_purchaser"])
                except Exception as e:
                    print("error en el cargo pero diferente a la api de conekta")
                    error.append(str(e.args))
                    print(e.args)
            else:
                print("Fallo al tokenizar la tarjeta")
                error.append("Fallo al tokenizar la tarjeta")

        else:
            print("error en el form")
            print(datosPedidoForm.errors)
            error.append(datosPedidoForm.errors)

    else:
        data = {
            "colonia": "aqui pondremos la direccion que puso como base",
            "ciudad": "aqui pondremos la direccion que puso como base"
        }
        if request.user.is_authenticated():
            id_user = request.user.id
            user = Usuario.objects.get(id=id_user)
            data["nombre"] = user.nombre
            data["apellidos"] = user.apellidos
            data["email"] = user.email
            data["telefono"] = user.telefono
        datosPedidoForm = formDatosPedido(
                data=data
        )
    return render(request, "pagos/paso2.html", {
        "datosPedido": datosPedidoForm,
        "error": error,
        "conektaLlavePublica": settings.CONEKTA_PUBLIC,
        "carrito": carrito,
        "TOTAL": total,
        "ENVIO": costoEnvio
    })
