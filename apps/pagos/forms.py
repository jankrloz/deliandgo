from django import forms
from django.utils.translation import ugettext_lazy as _

from apps.pagos.models import Tarjeta


class tarjetaForm(forms.ModelForm):
    class Meta:
        model = Tarjeta
        fields = (
            'nombre',
            'numero',
            'numero_secreto',
            'mes',
            'anio',
        )
        widgets = {
            'nombre': forms.TextInput(attrs={
                'required': 'true',
            }),
            'numero': forms.TextInput(attrs={
                'required': 'true',
            }),
            'numero_secreto': forms.TextInput(attrs={
                'type': 'password',
                'required': 'true',
            }),
            'mes': forms.TextInput(attrs={
                'required': 'true',
            }),
            'anio': forms.TextInput(attrs={
                'required': 'true',
            }),
        }
        labels = {
            'nombre': _("Nombre"),
            'numero': _("Numero"),
            'numero_secreto': _("Secreto"),
            'mes': _("MES"),
            'anio': _("AÑO"),
        }


class updateTarjetaForm(forms.Form):
    id_tarjeta = forms.IntegerField(
        widget=forms.NumberInput(attrs={
            "hidden": True
        })
    )
    id_tarjeta.required = True
    id_tarjeta.label = "tarjeta_id"

    nombre = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'required': 'true',
            }
        )
    )
    nombre.required = True
    nombre.label = "Nombre"

    numero = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'required': 'true',
            }
        )
    )
    numero.required = True
    numero.label = "Numero"

    numero_secreto = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'required': 'true',
                'type': 'password',
            }
        )
    )
    numero_secreto.required = True
    numero_secreto.label = "Secreto"

    mes = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'required': 'true',
            }
        )
    )
    mes.required = True
    mes.label = "MES"

    anio = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'required': 'true',
            }
        )
    )
    anio.required = True
    anio.label = "AÑO"


class formDatosPedido(forms.Form):
    nombre = forms.CharField(
        max_length=20,
        widget=forms.TextInput(
            attrs={
                'required': 'true',
            }
        )
    )
    nombre.required = True
    nombre.label = "Nombre"
    apellidos = forms.CharField(
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'required': 'true',
            }
        )
    )
    apellidos.required = True
    apellidos.label = "Apellidos"
    email = forms.EmailField(
        max_length=100,
        widget=forms.TextInput(
            attrs={
                "type": "email",
                'required': 'true',
            }
        )
    )
    email.required = True
    email.label = "Email"
    telefono = forms.CharField(
        min_length=13,
        widget=forms.TextInput(
            attrs={
                'required': 'true',
                "data-mask": "(000)000-0000",
                "title": "7-10 digitos",
            }
        )
    )
    telefono.required = True
    telefono.label = "Teléfono"
    direccion = forms.CharField(
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'required': 'true',
            }
        )
    )
    direccion.required = True
    direccion.label = "Dirección"
    numeroInt = forms.IntegerField(
        min_value=1,
        widget=forms.NumberInput(
            attrs={
                'required': 'true'
            }
        )
    )
    numeroInt.required = True
    numeroInt.label = "Número Interior"
    colonia = forms.CharField(
        max_length=200,
        widget=forms.TextInput(
            attrs={
                'disabled': "true",
            }
        )
    )
    colonia.required = False
    colonia.label = "Colonia"
    ciudad = forms.CharField(
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'disabled': "true",
            }
        )
    )
    ciudad.required = False
    ciudad.label = "Ciudad"
    referencia = forms.CharField(
        max_length=500,
        widget=forms.Textarea(
            attrs={
                'required': 'true',
                'rows': "4",
                'cols': "40"
            }
        )
    )
    referencia.required = True
    referencia.label = "Entre calles o referencia"
