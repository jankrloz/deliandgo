# -*- encoding: utf-8 -*-
from django.contrib import admin

from apps.usuarios.mailing import correo13
from .models import Usuario, Contacto, Restaurante, Direccion


# Register your models here.
@admin.register(Usuario)
class AdminUsuario(admin.ModelAdmin):
    list_display = (
        'email',
        'nombre',
        'apellidos',
        'telefono',
        'is_active',
        'is_restaurante',
    )
    fields = (
        'email',
        'username',
        'nombre',
        'apellidos',
        'fecha_registro',
        'telefono',
        'avatar',
        'is_active',
        'is_restaurante',
        'groups',
    )
    readonly_fields = (
        'fecha_registro',
    )
    filter_horizontal = (
        'groups',
    )


def RespuestaContacto(self, request, queryset):
    for q in queryset:
        contacto = Contacto.objects.get(id=q.id)
        if contacto.estado and contacto.restaurante_set.all().count() == 0:
            correo13(contacto)
    return queryset


@admin.register(Contacto)
class AdminContacto(admin.ModelAdmin):
    list_display = (
        'nombre_restaurante',
        'nombre_responsable',
        'telefono',
        'llaveRandom',
    )
    readonly_fields = (
        'fecha_hora',
        'llaveRandom',
    )
    actions = [RespuestaContacto, ]


@admin.register(Restaurante)
class AdminRestaurante(admin.ModelAdmin):
    list_display = (
        'contacto',
        'hora_abre',
        'hora_cierra',
    )


@admin.register(Direccion)
class AdminDireccion(admin.ModelAdmin):
    list_display = (
        'id',
        'usuario',
        'estado',
        'CP',
        'fecha_hora',
    )
    fields = (
        'usuario',
        'calle',
        'numero_ext',
        'numero_int',
        'estado',
        'CP',
        'referencia',
        'fecha_hora',
    )
    readonly_fields = (
        'fecha_hora',
    )
