from django.core import mail
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from django.utils.html import strip_tags

connection = mail.get_connection(backend="django.core.mail.backends.smtp.EmailBackend", use_tls=True,
                                 host="smtp.1and1.com", port=587, username="contacto@deliandgo.com",
                                 password="Zardoni1208")
connection2 = mail.get_connection(backend="django.core.mail.backends.smtp.EmailBackend", use_tls=True,
                                  host="smtp.1and1.com", port=587, username="clauglez@deliandgo.mx",
                                  password="Zardoni1208")


def correo01(destino=["isc4.tec@gmail.com", ]):
    try:
        connection.open()
        subject = "Bienvenido a Deli&Go"
        from_email = "Deli&Go <contaco@deliandgo.com>"
        to = destino
        url_accion = reverse('index')

        ctx = {
            "hola": "hola",
            "url_accion": url_accion,
        }
        html_content = render_to_string("mailing/correo01Bienvenido.html", context=ctx)
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, to, connection=connection)
        msg.attach_alternative(html_content, "txt/html")
        msg.send()
        connection.close()
    except Exception as e:
        print("error")
        print(e.args)


def correo02(destino=["isc4.tec@gmail.com", ]):
    try:
        connection.open()
        subject = "Bienvenido a Deli&Go"
        from_email = "Deli&Go <contaco@deliandgo.com>"
        to = destino
        url_accion = reverse('index')

        ctx = {
            "hola": "hola",
            "url_accion": url_accion,
        }
        html_content = render_to_string("mailing/correo02Pedido_en_camino.html", context=ctx)
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, to, connection=connection)
        msg.attach_alternative(html_content, "txt/html")
        msg.send()
        connection.close()
    except Exception as e:
        print("error")
        print(e.args)


def correo03(destino=["isc4.tec@gmail.com", ]):
    try:
        connection.open()
        subject = "Bienvenido a Deli&Go"
        from_email = "Deli&Go <contaco@deliandgo.com>"
        to = destino
        url_accion = reverse('index')

        ctx = {
            "hola": "hola",
            "url_accion": url_accion,
        }
        html_content = render_to_string("mailing/correo03Cargo_no_autorizado.html", context=ctx)
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, to, connection=connection)
        msg.attach_alternative(html_content, "txt/html")
        msg.send()
        connection.close()
    except Exception as e:
        print("error")
        print(e.args)


def correo04(destino=["isc4.tec@gmail.com", ]):
    try:
        connection.open()
        subject = "Bienvenido a Deli&Go"
        from_email = "Deli&Go <contaco@deliandgo.com>"
        to = destino
        url_accion = reverse('index')

        ctx = {
            "hola": "hola",
            "url_accion": url_accion,
        }
        html_content = render_to_string("mailing/correo04Orden_rechazada.html", context=ctx)
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, to, connection=connection)
        msg.attach_alternative(html_content, "txt/html")
        msg.send()
        connection.close()
    except Exception as e:
        print("error")
        print(e.args)


def correo05(destino=["isc4.tec@gmail.com", ]):
    try:
        connection.open()
        subject = "Bienvenido a Deli&Go"
        from_email = "Deli&Go <contaco@deliandgo.com>"
        to = destino
        url_accion = reverse('index')

        ctx = {
            "hola": "hola",
            "url_accion": url_accion,
        }
        html_content = render_to_string("mailing/correo05Pedido_cancelado.html", context=ctx)
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, to, connection=connection)
        msg.attach_alternative(html_content, "txt/html")
        msg.send()
        connection.close()
    except Exception as e:
        print("error")
        print(e.args)


def correo06(destino=["isc4.tec@gmail.com", ]):
    try:
        connection2.open()
        subject = "Bienvenido a Deli&Go"
        from_email = "Deli&Go <clauglez@deliandgo.com>"
        to = destino
        url_accion = reverse('index')

        ctx = {
            "hola": "hola",
            "url_accion": url_accion,
        }
        html_content = render_to_string("mailing/correo06Ofrece_tus_platillos.html", context=ctx)
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, to, connection=connection)
        msg.attach_alternative(html_content, "txt/html")
        msg.send()
        connection2.close()
    except Exception as e:
        print("error")
        print(e.args)


def correo07(destino=["isc4.tec@gmail.com", ]):
    try:
        connection2.open()
        subject = "Bienvenido a Deli&Go"
        from_email = "Deli&Go <clauglez@deliandgo.com>"
        to = destino
        url_accion = reverse('index')

        ctx = {
            "hola": "hola",
            "url_accion": url_accion,
        }
        html_content = render_to_string("mailing/correo07Creacion_de_restaurante.html", context=ctx)
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, to, connection=connection)
        msg.attach_alternative(html_content, "txt/html")
        msg.send()
        connection2.close()
    except Exception as e:
        print("error")
        print(e.args)


def correo08(destino=["isc4.tec@gmail.com", ]):
    try:
        connection2.open()
        subject = "Bienvenido a Deli&Go"
        from_email = "Deli&Go <clauglez@deliandgo.com>"
        to = destino
        url_accion = reverse('index')

        ctx = {
            "hola": "hola",
            "url_accion": url_accion,
        }
        html_content = render_to_string("mailing/correo08Bienvenido_restaurante.html", context=ctx)
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, to, connection=connection)
        msg.attach_alternative(html_content, "txt/html")
        msg.send()
        connection2.close()
    except Exception as e:
        print("error")
        print(e.args)


def correo09(destino=["isc4.tec@gmail.com", ]):
    try:
        connection2.open()
        subject = "Bienvenido a Deli&Go"
        from_email = "Deli&Go <clauglez@deliandgo.com>"
        to = destino
        url_accion = reverse('index')

        ctx = {
            "hola": "hola",
            "url_accion": url_accion,
        }
        html_content = render_to_string("mailing/correo09Tienes_un_pedido.html", context=ctx)
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, to, connection=connection)
        msg.attach_alternative(html_content, "txt/html")
        msg.send()
        connection2.close()
    except Exception as e:
        print("error")
        print(e.args)


def correo10(destino=["isc4.tec@gmail.com", ]):
    try:
        connection2.open()
        subject = "Bienvenido a Deli&Go"
        from_email = "Deli&Go <clauglez@deliandgo.com>"
        to = destino
        url_accion = reverse('index')

        ctx = {
            "hola": "hola",
            "url_accion": url_accion,
        }
        html_content = render_to_string("mailing/correo10Pago_semanal.html", context=ctx)
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, to, connection=connection)
        msg.attach_alternative(html_content, "txt/html")
        msg.send()
        connection2.close()
    except Exception as e:
        print("error")
        print(e.args)


def correo11(destino=["isc4.tec@gmail.com", ]):
    try:
        connection2.open()
        subject = "Bienvenido a Deli&Go"
        from_email = "Deli&Go <clauglez@deliandgo.com>"
        to = destino
        url_accion = reverse('index')

        ctx = {
            "hola": "hola",
            "url_accion": url_accion,
        }
        html_content = render_to_string("mailing/correo11Cambios_en_condiciones.html", context=ctx)
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, to, connection=connection)
        msg.attach_alternative(html_content, "txt/html")
        msg.send()
        connection2.close()
    except Exception as e:
        print("error")
        print(e.args)


def correo12(contacto):
    try:
        connection2.open()
        subject = "Informacion nuevo Contacto"
        from_email = "Deli&Go <clauglez@deliandgo.com>"
        to = ["clauglez@deliandgo.mx", ]
        url_accion = reverse('index')

        ctx = {
            "hola": "hola",
            "contacto": "contacto",
            "url_accion": url_accion,
        }
        html_content = render_to_string("mailing/correo12NuevoContacto.html", context=ctx)
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, to, connection=connection)
        msg.attach_alternative(html_content, "txt/html")
        msg.send()
        connection2.close()
    except Exception as e:
        print("error")
        print(e.args)


def correo13(contacto):
    try:
        connection2.open()
        subject = "Informacion pasaste revision"
        from_email = "Deli&Go <clauglez@deliandgo.com>"
        to = [contacto.email, ]
        url_accion = "http://localhost:8000/restaurante-datos/" + contacto.llaveRandom

        ctx = {
            "hola": "hola",
            "contacto": "contacto",
            "url_accion": url_accion,
        }
        html_content = render_to_string("mailing/correo13pasoChekeo.html", context=ctx)
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, to, connection=connection)
        msg.attach_alternative(html_content, "txt/html")
        msg.send()
        connection2.close()
    except Exception as e:
        print("error")
        print(e.args)
