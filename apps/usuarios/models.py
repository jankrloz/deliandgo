# -*- encoding: utf-8 -*-
import os
import random
import string

from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import PermissionsMixin, BaseUserManager, AbstractBaseUser
from django.db import models
from django.db.models.aggregates import Avg
from django.templatetags.static import static
from django.utils.text import slugify
from geoposition.fields import GeopositionField

from apps.catalogos.models import Tags, CuentaBanco
from apps.usuarios.mailing import correo01


def directorio_img_restaurante(instance, filename):
    return "restaurante/{id}/{file}".format(id=instance.slug, file=filename)


def directorio_img_usuarios(instance, filename):
    return "usuarios/{id}/{file}".format(id=instance.id, file=filename)


class UserManager(BaseUserManager, models.Manager):
    def _create_user(self, username, email, password, is_staff,
                     is_superuser, is_active=False, **extra_fields):
        print("METODO _create_user")
        print(username, email, password)
        email = self.normalize_email(email)
        # if not email:
        # raise ValueError('El email debe ser obligatorio')
        user = self.model(username=email, email=email, is_active=is_active,
                          is_staff=is_staff, is_superuser=is_superuser, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email, password=None, is_active=True, **extra_fields):
        print("CREANDO USUARIO NORMAL")
        print(username, email, password)
        print(extra_fields)
        correo01(destino=[email, ])
        return self._create_user(username=email, email=email, password=password,
                                 is_staff=False, is_superuser=False, is_active=True, **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        print("CREANDO SUPER USUARIO")
        print(username, email, password)
        print(extra_fields)
        return self._create_user(username=email, email=email, password=password,
                                 is_staff=True, is_superuser=True, is_active=True, **extra_fields)


class Usuario(AbstractBaseUser, PermissionsMixin):
    nombre = models.CharField(max_length=40, blank=True)
    apellidos = models.CharField(max_length=40, blank=True)
    fecha_registro = models.DateTimeField(auto_now_add=True)
    username = models.CharField(max_length=50, unique=True)
    email = models.EmailField(unique=True)
    avatar = models.FileField(blank=True, upload_to=directorio_img_usuarios)
    telefono = models.CharField(max_length=20, blank=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_restaurante = models.BooleanField(default=False)
    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    @property
    def get_short_name(self):
        if self.nombre != "":
            return self.nombre
        else:
            return self.email

    @property
    def get_full_name(self):
        if self.nombre != "":
            return self.nombre + " " + self.apellidos
        else:
            return str(self.email)

    @property
    def get_telefono(self):
        if self.telefono:
            return self.telefono
        else:
            return "0000000000"

    @property
    def urlImagen(self):
        if self.avatar:
            return self.avatar._get_url
        else:
            return static('base/img/default-avatar.jpg')

    @property
    def get_image_filename(self):
        return os.path.basename(self.imagen.name)

    @property
    def get_image_url(self):
        if self.avatar:
            return self.avatar.url
        else:
            from django.contrib.staticfiles.templatetags.staticfiles import static
            return static('base/img/default_avatar.jpg')

    def __unicode__(self):
        return self.get_full_name

    def __str__(self):
        return self.get_full_name


class Contacto(models.Model):
    nombre_restaurante = models.CharField(max_length=200)
    nombre_responsable = models.CharField(max_length=200)
    telefono = models.CharField(max_length=20)
    email = models.EmailField(max_length=200)
    fecha_hora = models.DateTimeField(auto_now_add=True)
    llaveRandom = models.CharField(max_length=20)
    direccion = GeopositionField()
    nombre_direccion = models.CharField(max_length=1000)
    url_yelp = models.CharField(max_length=1000, default="")
    estado = models.BooleanField(default=False)

    def __unicode__(self):
        return self.nombre_restaurante + " " + self.nombre_responsable + " " + str(self.telefono)

    def __str__(self):
        return self.nombre_restaurante + " " + self.nombre_responsable + " " + str(self.telefono)

    def save(self, *args, **kwargs):
        if not self.id:
            self.llaveRandom = genera_codigo()
        super(Contacto, self).save(*args, **kwargs)


class Restaurante(models.Model):
    duenio = models.ForeignKey(Usuario)
    contacto = models.ForeignKey(Contacto)
    #tags = models.ManyToManyField(Tags)
    slug = models.SlugField(max_length=200)
    cuenta_bancaria = models.ForeignKey(CuentaBanco, null=True)
    compra_minima = models.IntegerField(default=0)
    # distancia maxima a la que el restaurante puede llevar pedidos
    alcanze = models.IntegerField(default=1.0)
    lista_servicio = (
        ("Solo Domicilio", "Solo Domicilio"),
        ("Solo Local", "Solo Local"),
        ("Domicilio y Local", "Domicilio y Local"),
    )
    servicio = models.CharField(choices=lista_servicio, max_length=20, default="Domicilio y Local")
    hora_abre = models.TimeField()
    hora_cierra = models.TimeField()
    imagen = models.FileField(upload_to=directorio_img_restaurante)
    costo_envio = models.IntegerField(default=0)
    RFC = models.CharField(max_length=13)

    def __str__(self):
        return str(self.contacto)

    def __unicode__(self):
        return str(self.contacto)

    @property
    def get_name(self):
        return self.contacto.nombre_restaurante

    @property
    def urlImagen(self):
        if self.imagen:
            return self.imagen._get_url
        else:
            return static('base/img/default-avatar.jpg')

    @property
    def comentarioAvg(self):
        cantidad = 0
        resultado = self.comentarios_set.all()
        if resultado.exists():
            cantidad = resultado.aggregate(Avg('valoracion'))['valoracion__avg']
        return cantidad

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.contacto.nombre_restaurante)
        super(Restaurante, self).save(*args, **kwargs)


class Direccion(models.Model):
    usuario = models.ForeignKey(Usuario)
    calle = models.TextField()
    numero_ext = models.IntegerField()
    numero_int = models.IntegerField()
    estado = models.CharField(max_length=50)
    CP = models.CharField(max_length=5, default='00000')
    referencia = models.TextField()
    fecha_hora = models.DateTimeField(auto_now_add=True)

    @property
    def get_full(self):
        return "Usuario:" + self.usuario.get_full_name + " Principal " + self.calle + " No. Ext." + str(
                self.numero_ext) + " C.P." + str(
                self.CP) + " Estado: " + self.estado + " referencia: " + self.referencia

    def __unicode__(self):
        return self.calle + "/" + str(self.numero_ext) + "/" + str(self.CP) + "/" + self.estado

    def __str__(self):
        return self.calle + "/" + str(self.numero_ext) + "/" + str(self.CP) + "/" + self.estado


def genera_codigo():
    codigo_prueba = cadena_aleatorio(20)
    while existe_codigo(codigo_prueba):
        codigo_prueba = cadena_aleatorio(20)
    return codigo_prueba


def cadena_aleatorio(tamanio):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(tamanio))


def existe_codigo(codigo):
    return Contacto.objects.filter(llaveRandom=codigo).exists()


class EmailOrUsernameModelBackend(ModelBackend):
    def authenticate(self, username=None, password=None):
        if '@' in username:
            kwargs = {'email': username}
        else:
            kwargs = {'username': username}
        try:
            user = Usuario.objects.get(**kwargs)
            if user.check_password(password):
                return user
        except Usuario.DoesNotExist as e:
            return None

    def get_user(self, username):
        try:
            return Usuario.objects.get(pk=username)
        except Usuario.DoesNotExist as e:

            return None

    def __str__(self):
        return str(self)

    @property
    def get_rating(self):
        return 0
