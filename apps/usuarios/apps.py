# -*- encoding: utf-8 -*-
from django.apps import AppConfig


class UsuariosConfig(AppConfig):
    name = 'apps.usuarios'
