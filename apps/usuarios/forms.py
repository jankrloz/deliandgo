# -*- encoding: utf-8 -*-

from django import forms
from django.conf import settings
from django.contrib.auth.forms import PasswordResetForm
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from geoposition.forms import GeopositionField

from apps.catalogos.models import Tags
from apps.usuarios.models import Usuario, Contacto, Restaurante, Direccion


class EmailValidationOnForgotPassword(PasswordResetForm):
    def clean_email(self):
        email = self.cleaned_data['email']
        if not Usuario.objects.filter(email__iexact=email, is_active=True).exists():
            raise ValidationError("No tenemos tu correo registrado, o tu cuenta aun no ha sido validada ")

        return email


class LoginForm(forms.Form):
    email = forms.CharField(max_length=50,
                            required=True,
                            label='Correo electrónico',
                            widget=forms.TextInput(attrs={
                                'required': 'true',
                            }))
    password = forms.CharField(max_length=50,
                               required=True,
                               label='Password',
                               widget=forms.TextInput(attrs={
                                   'type': 'password',
                                   'required': 'true',
                               }))

    recordarme = forms.BooleanField(required=False,
                                    widget=forms.CheckboxInput())

    def clean(self):
        recordarme = self.cleaned_data.get('recordarme')
        if not recordarme:
            settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = True
        else:
            settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = False

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        self.fields['password'].required = True


class SignUpForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ('email', 'password')
        widgets = {
            'email': forms.EmailInput(attrs={
                'type': 'email',
                'class': 'form-control',
                'required': 'true',
            }),
            'password': forms.TextInput(attrs={
                'type': 'password',
                'class': 'form-control',
                'required': 'true',
            }),
        }
        labels = {
            'email': _("Correo electrónico"),
            'password': _("Crea una contraseña"),
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        correo = True
        if len(cleaned_data.get("password")) < 6:
            self._errors['password'] = self.error_class(["La contraseña debe contener mínimo 6 caracteres"])
        if Usuario.objects.filter(email__iexact=cleaned_data.get("email").lower()).exists():
            self._errors['email'] = self.error_class(["El correo ya esta en uso ..."])
            correo = False

        if correo:
            return cleaned_data

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        self.fields['password'].required = True


class ContactoForm(forms.ModelForm):
    class Meta:
        model = Contacto
        fields = (
            'nombre_restaurante',
            'nombre_responsable',
            'telefono',
            'email',
        )
        widgets = {
            'nombre_restaurante': forms.TextInput(attrs={
                'required': 'true',
                'class': 'form-control'
            }),
            'nombre_responsable': forms.TextInput(attrs={
                'required': 'true',
                'class': 'form-control'
            }),
            'telefono': forms.TextInput(attrs={
                'required': 'true',
                "minlength": "10",
                "maxlength": "12",
                "data-mask": "(000)000-0000",
                "title": "7-10 digitos",
                'class': 'form-control'
            }),
            'email': forms.EmailInput(attrs={
                'type': 'email',
                'required': 'true',
                'class': 'form-control'
            }),
        }

        labels = {
            'nombre_restaurante': _("Nombre del restaurante"),
            'nombre_responsable': _("Nombre del contacto"),
            'telefono': _("Teléfono"),
            'email': _("Correo electrónico"),
        }

    direccion = GeopositionField(
            initial="19.3911668,-99.4238148"
    )
    direccion.label = ""
    '''
    def __init__(self, *args, **kwargs):
        self.fields['nombre_restaurante'].required = True
        self.fields['nombre_responsable'].required = True
        self.fields['email'].required = True
        self.fields['telefono'].required = True

        super(ContactoForm, self).__init__(*args, **kwargs)
    '''


class DatosExtraUsuarioForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = (
            'nombre',
            'apellidos',
            'telefono',
            'avatar',
        )
        widgets = {
            'nombre': forms.TextInput(attrs={
                'required': 'true',
                'class': 'form-control'
            }),
            'apellidos': forms.TextInput(attrs={
                'required': 'true',
                'class': 'form-control'
            }),
            'telefono': forms.TextInput(attrs={
                'required': 'true',
                "minlength": "10",
                "maxlength": "12",
                "data-mask": "(000)000-0000",
                "title": "7-10 digitos",
                'class': 'form-control'
            }),
            'avatar': forms.FileInput(attrs={
                'required': 'true',
                'type': 'file',
                'accept': 'image/*',
                'class': 'form-control file'
            }),
        }
        labels = {
            'nombre': _("Nombre"),
            'apellidos': _("Apellidos"),
            'telefono': _("Telefono"),
            'avatar': _("Imagen Perfil"),
        }


class RestauranteForm(forms.ModelForm):
    class Meta:
        model = Restaurante
        fields = (
            "compra_minima",
            "alcanze",
            "hora_abre",
            "hora_cierra",
            "imagen",
            "servicio",
            "costo_envio",
            "RFC",
        )
        widgets = {
            'compra_minima': forms.TextInput(attrs={
                'type': 'number',
                "min": "0",
                'required': 'true',
                'class': 'form-control'
            }),
            'alcanze': forms.TextInput(attrs={
                'type': 'number',
                "min": "1",
                'required': 'true',
                'class': 'form-control'
            }),
            'hora_abre': forms.TextInput(attrs={
                "type": "time",
                'required': 'true',
                'class': 'form-control timepicker'
            }),
            'hora_cierra': forms.TextInput(attrs={
                "type": "time",
                'required': 'true',
                'class': 'form-control timepicker'
            }),
            'costo_envio': forms.TextInput(attrs={
                'type': 'number',
                "min": "0",
                'required': 'true',
                'class': 'form-control'
            }),
            'RFC': forms.TextInput(attrs={
                "minlength": "12",
                'required': 'true',
                "mask": "LLLXNNNNNXXXX, {'translation': {L: {pattern: /[A-Za-z]/},N: {pattern: /[0-9]/},X: {pattern: /[A-Za-z0-9]/}}});",
                'class': 'form-control'
            }),

            'servicio': forms.Select(attrs={
                'class': 'form-control'
            })

        }
        labels = {
            "compra_minima": _("Compra Minima ($)"),
            "alcanze ": _("Alcanze de servicio a domicilio (KM)"),
            "hora_abre": _("¿A qué hora abre?"),
            "hora_cierra": _("¿A qué hora cierra?"),
            "hora_imagen": _("Imagen principal"),
            "servicio": _("Tipo de servicio"),
            "costo_envio": _("Costo de Envio a Domicilio"),
            "RFC": _("R F C"),
        }

    '''
    tags = forms.ModelMultipleChoiceField(
        queryset=Tags.objects.filter(isPrincipal=True))
    tags.label = "Estilo del restaurante"
    tags.required = True
    '''

    def __init__(self, *args, **kwargs):
        super(RestauranteForm, self).__init__(*args, **kwargs)
        self.fields['compra_minima'].required = True
        self.fields['alcanze'].required = True
        self.fields['hora_abre'].required = True
        self.fields['hora_cierra'].required = True
        self.fields['imagen'].required = True
        self.fields['servicio'].required = True
        self.fields['RFC'].required = True


class updateInfoRestauranteForm(forms.Form):
    tags = forms.ModelMultipleChoiceField(
            queryset=Tags.objects.all())
    tags.label = "Estilo del restaurante"
    tags.required = True
    servicio = forms.ChoiceField(
            widget=forms.RadioSelect,
            choices=Restaurante.lista_servicio,
    )
    servicio.required = True
    servicio.label = "Tipo de servicio"

    nombre_restaurante = forms.CharField()
    nombre_restaurante.label = "Nombre del restaurante"
    nombre_restaurante.required = True

    telefono = forms.CharField(
            widget=forms.TextInput(attrs={
                'required': 'true',
                "minlength": "10",
                "maxlength": "12",
                "data-mask": "(000)000-0000",
                "title": "7-10 digitos",
            })
    )
    telefono.label = "Telefono"
    telefono.required = True

    email = forms.EmailField()
    email.label = "Correo electronico"
    email.required = True

    compra_minima = forms.IntegerField()
    compra_minima.label = "Compra minima ($)"
    compra_minima.required = True

    alcanze = forms.IntegerField()
    alcanze.label = "Alcance (KM)"
    alcanze.required = True

    costo_envio = forms.IntegerField()
    costo_envio.label = "Costo de envio ($)"
    costo_envio.required = True

    hora_abre = forms.TimeField()
    hora_abre.label = "Hora en que abre"
    hora_abre.required = True

    hora_cierra = forms.TimeField()
    hora_cierra.label = "Hora en que cierra"
    hora_cierra.required = True


class updateImagenRestauranteForm(forms.Form):
    slug = forms.CharField(
            widget=forms.TextInput(attrs={
                "hidden": True
            })
    )
    slug.required = True
    slug.label = "slug"

    imagen = forms.FileField(widget=forms.FileInput(attrs={
        'class': 'file form-control',
        'accept': 'image/*'
    }))
    imagen.required = True
    imagen.label = "Imagen"


class updateImagenPlatilloForm(forms.Form):
    id_platillo = forms.IntegerField(
            widget=forms.NumberInput(attrs={
                "hidden": True
            })
    )
    id_platillo.required = True
    id_platillo.label = "id_platillo"
    imagen = forms.FileField(widget=forms.FileInput(attrs={
        'class': 'form-control file',
        'accept': 'image/*'
    }))
    imagen.required = True
    imagen.label = "Imagen"


class updateUsuarioForm(forms.Form):
    nombre = forms.CharField(
            max_length=40,
            required=True,
            label='Nombre',
            widget=forms.TextInput(
                    attrs={
                        'required': 'true',
                        'class': 'form-control'
                    }
            )
    )
    apellidos = forms.CharField(
            max_length=40,
            required=True,
            label='Apellidos',
            widget=forms.TextInput(
                    attrs={
                        'required': 'true',
                        'class': 'form-control'
                    }
            )
    )
    telefono = forms.CharField(
            max_length=40,
            required=True,
            label='Telefono',
            widget=forms.TextInput(
                    attrs={
                        'required': 'true',
                        "minlength": "10",
                        "maxlength": "12",
                        "data-mask": "(000)000-0000",
                        "title": "7-10 digitos",
                        'class': 'form-control'
                    }
            )
    )


class updateImgUsuarioForm(forms.Form):
    id_usuario = forms.IntegerField(
            widget=forms.NumberInput(attrs={
                "hidden": True
            })
    )
    id_usuario.required = True
    id_usuario.label = "id_usuario"
    imagen = forms.FileField(widget=forms.FileInput(attrs={
        'class': 'form-control file',
        'accept': 'image/*'
    }))
    imagen.required = True
    imagen.label = "Imagen"


estados = [
    ("Aguascalientes", _('Aguascalientes')),
    ("Baja California", _('Baja California')),
    ("Baja California Sur", _('Baja California Sur')),
    ("Campeche", _('Campeche')),
    ("Chiapas", _('Chiapas')),
    ("Chihuahua", _('Chihuahua')),
    ("Coahuila", _('Coahuila')),
    ("Colima", _('Colima')),
    ("Distrito Federal", _('Distrito Federal')),
    ("Durango", _('Durango')),
    ("Estado de México", _('Estado de México')),
    ("Guanajuato", _('Guanajuato')),
    ("Guerrero", _('Guerrero')),
    ("Hidalgo", _('Hidalgo')),
    ("Jalisco", _('Jalisco')),
    ("Michoacán", _('Michoacán')),
    ("Morelos", _('Morelos')),
    ("Nayarit", _('Nayarit')),
    ("Nuevo León", _('Nuevo León')),
    ("Oaxaca", _('Oaxaca')),
    ("Puebla", _('Puebla')),
    ("Querétaro", _('Querétaro')),
    ("Quintana Roo", _('Quintana Roo')),
    ("San Luis Potosí", _('San Luis Potosí')),
    ("Sinaloa", _('Sinaloa')),
    ("Sonora", _('Sonora')),
    ("Tabasco", _('Tabasco')),
    ("Tamaulipas", _('Tamaulipas')),
    ("Tlaxcala", _('Tlaxcala')),
    ("Veracruz", _('Veracruz')),
    ("Yucatán", _('Yucatán')),
    ("Zacatecas", _('Zacatecas'))
]


class direccionForm(forms.ModelForm):
    class Meta:
        model = Direccion

        fields = (
            'calle',
            'numero_ext',
            'numero_int',
            'estado',
            'CP',
            'referencia',
        )
        widgets = {
            'calle': forms.TextInput(
                    attrs={
                        'required': 'true',
                    }
            ),
            'numero_ext': forms.NumberInput(
                    attrs={
                        'required': 'true',
                    }
            ),
            'numero_int': forms.NumberInput(
                    attrs={
                        'required': 'true',
                    }
            ),
            'estado': forms.Select(
                    choices=estados,
            ),
            'CP': forms.NumberInput(
                    attrs={
                        'required': 'true',
                    }
            ),
            'referencia': forms.TextInput(
                    attrs={
                        'required': 'true',
                    }
            ),
        }
        labels = {
            'calle': "Calle Principal",
            'numero_ext': "No. Exterior",
            'numero_int': "No. Interior",
            'estado': "Estado",
            'CP': "Codigo Postal",
            'referencia': "Referencia",
        }


class updateDireccionForm(forms.Form):
    id_direccion = forms.IntegerField(
            widget=forms.NumberInput(attrs={
                "hidden": True
            })
    )
    id_direccion.required = True
    id_direccion.label = "id_direccion"

    calle = forms.CharField()
    calle.required = True
    calle.label = "Calle Principal"

    numero_ext = forms.CharField()
    numero_ext.required = True
    numero_ext.label = "No. Ext."

    numero_int = forms.CharField()
    numero_int.required = True
    numero_int.label = "No. Int."

    estado = forms.ChoiceField(
            choices=estados
    )
    estado.required = True
    estado.label = "Estado"

    CP = forms.CharField(
            max_length=5,
    )
    CP.required = True
    CP.label = "Codigo Postal"

    referencia = forms.CharField()
    referencia.required = True
    referencia.label = "Referencia"
