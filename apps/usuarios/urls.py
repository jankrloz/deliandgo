# -*- encoding: utf-8 -*-

from django.conf.urls import url

urlpatterns = [
    # Examples:
    # url(r'^$', 'apps.usuarios.views.usuarioPerfil', name='usuarioPerfil'),
    url(r'login/$', 'apps.usuarios.views.LogIn', name='LogIn'),
    url(r'logout/$', 'apps.usuarios.views.LogOut', name='LogOut'),
    url(r'preguntas-frecuentes/$', 'apps.usuarios.views.faq', name='faq'),
    url(r'condiciones/$', 'apps.usuarios.views.condiciones', name='condiciones'),
    url(r'perfil/$', 'apps.servicios.views.PerfilUsuario', name='Perfil'),
    url(r'ajax_validar_correo/$', 'apps.usuarios.views.ajax_validar_correo', name='ajax_validar_correo'),
    url(r'ajax_actualizar_pedidos/$', 'apps.usuarios.views.ajax_actualizar_pedidos', name='ajax_actualizar_pedidos'),
    url(r'contacto/$', 'apps.usuarios.views.contactoRestaurante', name='Contacto'),
    url(r'mis-datos/$', 'apps.usuarios.views.datosExtra', name='datosExtra'),
    url(r'restaurante-datos/(?P<llave>\w+)/$', 'apps.usuarios.views.agregarRestaurante', name='addRestaurante'),
    url(r'restaurante/(?P<slug>[\w-]+)/$', 'apps.usuarios.views.detalleRestaurante', name='detalleRestaurante'),
    url(r'control-restaurantes/$', 'apps.usuarios.views.testGlovalV2', name='controlRestaurantes'),
    url(r'test/$', 'apps.usuarios.views.testGlovalV2', name='test'),
]
