# -*- encoding: utf-8 -*-
import json
from datetime import date

import requests
from django.conf import settings
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http.response import Http404, HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from geoposition import Geoposition

from apps.catalogos.models import Tags
from apps.estadisticas.forms import ComentraioForm
from apps.estadisticas.functions import guardarMetadatos
from apps.estadisticas.models import Comentarios
from apps.servicios.functions import busquedaCiclada, buscar_existe_en_yelp, calcular_gastos_por_semana, \
    dame_semanas_disponibles
from apps.servicios.models import Platillo, Pedido
from apps.usuarios.forms import LoginForm, SignUpForm, ContactoForm, DatosExtraUsuarioForm, RestauranteForm
from apps.usuarios.mailing import correo07, correo08, correo12
from apps.usuarios.models import Usuario, Contacto, EmailOrUsernameModelBackend, Restaurante


def faq(request):
    return render(request, 'usuarios/faq.html')


def condiciones(request):
    return render(request, 'usuarios/condiciones.html')


def contactoRestaurante(request):
    guardarMetadatos(request)
    if request.method == "POST":
        formContacto = ContactoForm(request.POST)
        # print(request.POST)
        if formContacto.is_valid():
            try:
                contacto = formContacto.save()
                if "direccion_0" in request.POST and "direccion_1" in request.POST:
                    try:
                        lat = float(request.POST["direccion_0"])
                        lon = float(request.POST["direccion_1"])
                        geo = Geoposition(
                                lat,
                                lon
                        )
                        contacto.direccion = geo
                        print(geo)
                        contacto.save()
                        r = requests.get(
                                "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + str(lat) + "," + str(
                                        lon) + "&sensor=true")
                        contacto.nombre_direccion = (r.json())["results"][0]["formatted_address"]
                        contacto.save()
                        contacto.url_yelp = buscar_existe_en_yelp(
                                busqueda=contacto.nombre_restaurante,
                                latitudOri=contacto.direccion.latitude,
                                longitudOri=contacto.direccion.longitude,
                                telefono=contacto.telefono
                        )
                        contacto.save()
                        print("se salvo la posicion de contacto")
                    except Exception as e:
                        print("error geoposicion")
                        print(e.args)
                    correo12(contacto)
                return redirect(reverse('index'))
            except Exception as e:
                print(e.args)
    formContacto = ContactoForm()
    return render(request, "usuarios/contactoRestautante.html", {
        'form_contacto': formContacto
    })


def LogIn(request):
    guardarMetadatos(request)
    user_register = SignUpForm()
    login_form = LoginForm()
    register_error = False
    url_redireccion = request.GET['next'] if 'next' in request.GET else '/'
    print(url_redireccion)
    print(vars(redirect(url_redireccion)))
    print(request.POST)
    print(request.GET)
    if request.user.is_authenticated():
        return redirect(url_redireccion)
    if request.method == 'POST':
        if request.session.test_cookie_worked():
            request.session.delete_test_cookie()
        else:
            pass
        if 'register_form' in request.POST:
            print("vamo a registrarno")
            user_register = SignUpForm(request.POST)
            if user_register.is_valid():
                Usuario.objects.create_user(
                        username=user_register.cleaned_data['email'].lower().strip().replace(' ', '_'),
                        email=user_register.cleaned_data['email'].lower(),
                        password=user_register.cleaned_data['password'],
                        is_active=True)

                autentificar = EmailOrUsernameModelBackend()
                user = autentificar.authenticate(
                        username=user_register.cleaned_data['email'].lower().strip().replace(' ', '_'),
                        password=user_register.cleaned_data['password'])
                print("imprimiendo al usuario")
                print(user)
                if user is not None:
                    user.backend = 'django.contrib.auth.backends.ModelBackend'
                    login(request, user)
                    if url_redireccion == "/":
                        return redirect(reverse("usuarios_app:datosExtra"))
                    return redirect(url_redireccion)

            register_error = True

        if 'login_form' in request.POST:
            print("vamos a logearno")
            login_form = LoginForm(request.POST)
            autentificar = EmailOrUsernameModelBackend()
            user = autentificar.authenticate(
                    username=request.POST['username'].lower().strip(),
                    password=request.POST['password'])
            print(user)
            if not "recordarme" in request.POST:
                settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = True
            else:
                settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = False
            if user is not None:
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                login(request, user)
                if url_redireccion == "/" and user.is_restaurante:
                    print("no va a ningun lugar y tiene restaurante")
                    restaurantes = Restaurante.objects.filter(duenio=user)
                    print(restaurantes)
                    print(restaurantes.first().slug)
                    if restaurantes.exists():
                        return redirect(reverse('servicios_app:perfilRestaurante', kwargs={
                            'slug': restaurantes.first().slug
                        }))
                return redirect(url_redireccion)
            else:
                return render(request, "usuarios/login.html",
                              {'user_register': user_register,
                               'login_form': login_form,
                               'error': "El nombre de usuario o contraseña son incorrectos"})
    request.session.set_test_cookie()
    return render(request, "usuarios/login.html", {
        'user_register': user_register,
        'login_form': login_form,
        'register_error': register_error,})


def datosExtra(request):
    # guardarMetadatos(request)
    print("post")
    print(request.POST)
    print("files")
    print(request.FILES)
    print("fin de todo")
    user = request.user
    if user.is_authenticated():
        if request.method == "POST":
            perfilForm = DatosExtraUsuarioForm(request.POST, request.FILES)
            print("salva")
            if perfilForm.is_valid():
                try:
                    usuario = Usuario.objects.get(id=user.id)
                    usuario.nombre = perfilForm.cleaned_data['nombre']
                    usuario.apellidos = perfilForm.cleaned_data['apellidos']
                    usuario.telefono = perfilForm.cleaned_data['telefono']
                    usuario.avatar = request.FILES["avatar"]
                    usuario.save()
                    print("se salvo")
                    return redirect(reverse('usuarios_app:Perfil'))
                except Exception as e:
                    print(e.args)
            else:
                return render(request, "usuarios/otrosDatos.html", {
                    "form": perfilForm,
                    "error": perfilForm.errors
                })
        perfilForm = DatosExtraUsuarioForm()
        return render(request, "usuarios/otrosDatos.html", {"form": perfilForm})
    else:
        return redirect(reverse('usuarios_app:LogIn'))


@login_required
def agregarRestaurante(request, llave):
    print("request.POST")
    print(request.POST)
    print("request.Files")
    print(request.FILES)
    print("request.Get")
    print(request.GET)
    print("fin request")
    if len(llave) != 20:
        raise Http404("intento entrar con una llave de distinta longitud")
    else:
        formRestaurante = RestauranteForm()
        contacto = Contacto.objects.filter(llaveRandom=llave)

        if contacto.exists():
            contacto = contacto.first()
            if request.method == "POST":
                formRestaurante = RestauranteForm(request.POST, request.FILES)
                print(request.POST)
                print(formRestaurante.is_valid())
                if formRestaurante.is_valid():
                    restaurante = formRestaurante.save(commit=False)
                    restaurante.contacto = contacto
                    usuario = request.user
                    restaurante.duenio = usuario
                    restaurante.save()
                    if "tags" in request.POST:
                        lista = []
                        for id in request.POST.getlist('tags'):
                            lista.append(int(id))
                        try:
                            tags = Tags.objects.filter(id__in=lista)
                            restaurante.tags = tags
                        except Exception as e:
                            print("Erro al guardar los tags")
                            print(e.args)
                    restaurante.save()
                    contacto.llaveRandom = ""
                    contacto.save()
                    usuario.is_restaurante = True
                    usuario.save()
                    correo07(destino=[usuario.email, ])
                    correo08(destino=[usuario.email, ])
                    try:
                        busquedaCiclada(restaurante)
                    except Exception as e:
                        print(e.args)
                    return redirect(reverse('servicios_app:perfilRestaurante', kwargs={
                        'slug': restaurante.slug
                    }))
                else:
                    print(formRestaurante.errors)
            return render(request, "usuarios/DatosRestaurante.html", {
                "form": formRestaurante
            })
        else:
            raise Http404("la llave con la que se intenta entrar no es valida")


def LogOut(request):
    logout(request)
    return redirect(reverse('usuarios_app:LogIn'))


def detalleRestaurante(request, slug):
    restaurantes = Restaurante.objects.filter(slug=slug)
    if restaurantes.exists():
        restaurante = restaurantes.first()
        platillos = Platillo.objects.filter(restaurante=restaurantes)
        user = request.user
        puedeComentar = False
        if user.is_authenticated():
            puedeComentar = Pedido.objects.filter(platillo__restaurante=restaurante, usuario=user.id)
        comentarioForm = ComentraioForm()
        if request.method == "POST":
            comentarioForm = ComentraioForm(request.POST)
            print(request.POST)
            if comentarioForm.is_valid():
                Comentarios.objects.create(
                        restaurante=restaurante,
                        usuario=request.user.id,
                        nombre=request.user.get_full_name,
                        valoracion=int(request.POST["calificacion"]),
                        texto=request.POST["comentario"],
                )
                return redirect(reverse("usuarios_app:detalleRestaurante", kwargs={'slug': slug}))
        return render(request, "servicios/detalleRestaurante.html", {
            "restaurante": restaurante,
            "platillos": platillos,
            "comentario_form": comentarioForm,
            "puede_comentar": puedeComentar,
        })
    else:
        return redirect(reverse('index'))


@csrf_exempt
def ajax_validar_correo(request):
    print("post")
    print(request.POST)
    print("fin")
    if request.is_ajax():
        if request.method == 'POST':
            email = request.POST["email"]
            print(email)
            print(Usuario.objects.filter(email=email).exists())
            return HttpResponse(Usuario.objects.filter(email=email).exists())
    return HttpResponse(False)


@csrf_exempt
def ajax_actualizar_pedidos(request):
    # print("post")
    # print(request.POST)
    # print("fin")
    if request.is_ajax():
        if request.method == 'POST':
            user1 = int(request.POST["usuario"])

            # print(user1)
            user2 = int(request.user.id)
            # print(user2)
            if user1 == user2:
                pedidos = Pedido.objects.filter(usuario=user1)
                # print(pedidos)
                resultado = []
                for pedido in pedidos:
                    diccionarioHijo = {}
                    diccionarioHijo["id"] = pedido.id
                    diccionarioHijo["estado"] = pedido.estado
                    resultado.append(diccionarioHijo)
                diccionario = {"resultados": resultado}
                # print(diccionario)
                diccionario = json.dumps(diccionario)
                return HttpResponse(diccionario)
    return HttpResponse(False)


@login_required
def controlRestaurantes(request):
    user = request.user
    restaurantes = Restaurante.objects.all()
    mes = date.today().month
    anio = date.today().year
    for restaurante in restaurantes:
        montoPedidos = 0
        lista = []
        for movimiento in restaurante.movimiento_set.filter(fecha__month=mes, fecha__year=anio):
            movimiento.comision = round(movimiento.monto * .085, 2)
            movimiento.balanze = round(movimiento.monto - movimiento.comision, 2)
            montoPedidos += movimiento.monto
            lista.append(movimiento)
        restaurante.movimientos = lista
        restaurante.montoPedidos = round(montoPedidos, 2)
        restaurante.multas = 0.0
        restaurante.cuenta = "XXXX-XXXX-XXXX-XXXX"
    return render(request, "superusuario/VistaGlobal.html", {
        "restaurantes": restaurantes,
    })
    '''
    if user.is_superuser:
        restaurantes = Restaurante.objects.all()
        mes = date.today().month
        anio = date.today().year
        for restaurante in restaurantes:
            restaurante.movimiento_set = restaurante.movimiento_set.filter(fecha__month=mes, fecha__year=anio)
            balanze = 0
            montoPedidos = 0
            for movimiento in restaurante.movimiento_set.all():
                movimiento.comision = round(movimiento.monto * .085, 3)
                balanze += round(movimiento.monto - movimiento.comision, 2)
                movimiento.balanze = balanze
                montoPedidos += movimiento.monto
            restaurante.montoPedidos = montoPedidos
            restaurante.multas = 0
            restaurante.cuenta = "XXXX-XXXX-XXXX-XXXX"
        return render(request, "superusuario/VistaGlobal.html", {
            "restaurantes": restaurantes,
        })
    else:
        logout(request)
        return redirect(reverse("servicios_app:buscador2"))
    '''


def testGloval(request):
    datos = calcular_gastos_por_semana()
    return render(request, "superusuario/test.html", {
        "datos": datos
    })


def testGlovalV2(request):
    semanas = dame_semanas_disponibles()
    return render(request, "superusuario/test2.html", {
        "semanas": semanas
    })
