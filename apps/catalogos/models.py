# -*- encoding: utf-8 -*-
from django.db import models


# Create your models here.
class Tags(models.Model):
    nombre = models.CharField(max_length=20, unique=True)
    nombre_icono = models.CharField(max_length=64, null=True, blank=True)

    def __unicode__(self):
        return self.nombre

    def __str__(self):
        return self.nombre


class CuentaBanco(models.Model):
    numero = models.CharField(max_length=16, null=False, blank=False)
    banco = models.CharField(max_length=50, null=False, blank=False)

    def __unicode__(self):
        return self.banco + "-" + str(self.numero)

    def __str__(self):
        return self.banco + "-" + str(self.numero)


class Pregunta(models.Model):
    texto = models.CharField(max_length=200)
    lista_tipo = (
        ("text", "text"),
        ("boolean", "boolean"),
        ("int", "int"),
    )
    tipo = models.CharField(choices=lista_tipo, max_length=10)

    def __str__(self):
        return self.texto + " " + self.tipo

    def __unicode__(self):
        return self.texto + " " + self.tipo
