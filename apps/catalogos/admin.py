# -*- encoding: utf-8 -*-
from django.contrib import admin

from apps.catalogos.models import CuentaBanco, Tags, Pregunta


@admin.register(Tags)
class AdminTags(admin.ModelAdmin):
    list_display = (
        'nombre',
    )
    list_editable = (
        'nombre',
    )
    search_fields = (
        'nombre',
    )


@admin.register(CuentaBanco)
class AdminCuentaBanco(admin.ModelAdmin):
    pass


@admin.register(Pregunta)
class AdminPregunta(admin.ModelAdmin):
    pass
